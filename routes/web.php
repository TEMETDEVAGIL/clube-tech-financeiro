<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('index');
});

Route::get('uploadBanner', function () {
    return view('Uploads/uploadBanner');
});

Route::get('uploadCurso', function () {
    return view('Uploads/uploadCurso');
});

Route::get('uploadBlog', function () {
    return view('Uploads/uploadBlog');
});

Route::get('uploadProfessor', function () {
    return view('Uploads/uploadProfessor');
});


Route::get('relatorioscontaspagas', function () {
    return view('RelatoriosContasPagas');
});

Route::get('relatorioscontasrecebidas', function () {
    return view('RelatorioContasRecebidas');
});

Route::get('curso', function () {
    return view('curso');
});

Route::get('curriculo', function () {
    return view('curriculo');
});

//limpar cache servidor

/*
Route::get('/clear-cache', function() {
    $run = Artisan::call('config:clear');
    $run = Artisan::call('cache:clear');
    $run = Artisan::call('config:cache');
    return 'FINISHED';  
});
*/

//Envio  Info Curso
Route::post('curso','CursoController@curso')->name('curso.curso');

//Envio de email
Route::post('email','LeadController@save')->name('lead.save');

//cadastro  Depoimento
Route::post('depoimento','DepoimentoController@savedepoimento')->name('depoimento.save');

//UPLOAD DE ARQUIVOS PARA O LOCAL STORAGE Banners
Route::post('uploadbanner','UploadController@uploadBanner');

//UPLOAD DE ARQUIVOS PARA O LOCAL STORAGE Cursos
Route::post('uploadcurso','UploadController@uploadCurso');

//UPLOAD DE ARQUIVOS PARA O LOCAL STORAGE Blogs
Route::post('uploadblog','UploadController@uploadBlog');

//UPLOAD DE ARQUIVOS PARA O LOCAL STORAGE professores
Route::post('uploadprofessor','UploadController@uploadProfessor');


//UPLOAD DE ARQUIVOS PARA O LOCAL STORAGE
Route::post('curriculo','CurriculoController@upload');

//ROTA ENVIO FORM INSCRIÇÃO PROFESSOR
Route::post('inscricao', 'InscricaoController@save')->name('inscricao.save');

//ROTAS BANCO/VIEW HOME
Route::get('/','IndexController@index');


//RELATORIO CONTAS PAGAS

//Relatorio Janeiro
Route::get('relatoriojaneiropaga', 'PdfContaspagasController@relatoriojaneiro')->name('pdf');

//Relatorio Fevereiro
Route::get('relatoriofevereiropaga', 'PdfContaspagasController@relatoriofevereiro')->name('pdf');

//Relatorio Março
Route::get('relatoriomarcopaga', 'PdfContaspagasController@relatoriomarco')->name('pdf');

//Relatorio Abril
Route::get('relatorioabrilpaga', 'PdfContaspagasController@relatorioabril')->name('pdf');

//Relatorio Maio
Route::get('relatoriomaiopaga', 'PdfContaspagasController@relatoriomaio')->name('pdf');

//Relatorio Junho
Route::get('relatoriojunhopaga', 'PdfContaspagasController@relatoriojunho')->name('pdf');

//Relatorio Julho
Route::get('relatoriojulhopaga', 'PdfContaspagasController@relatoriojulho')->name('pdf');

//Relatorio Agosto
Route::get('relatorioagostopaga', 'PdfContaspagasController@relatorioagosto')->name('pdf');

//Relatorio Setembro
Route::get('relatoriosetembropaga', 'PdfContaspagasController@relatoriosetembro')->name('pdf');

//Relatorio Outubro
Route::get('relatoriooutubropaga', 'PdfContaspagasController@relatoriooutubro')->name('pdf');

//Relatorio Novembro
Route::get('relatorionovembropaga', 'PdfContaspagasController@relatorionovembro')->name('pdf');

//Relatorio Dezembro
Route::get('relatoriodezembropaga', 'PdfContaspagasController@relatoriodezembro')->name('pdf');


//RELATORIO CONTAS RECEBIDAS

//Relatorio Janeiro
Route::get('relatoriojaneirorecebido', 'PdfContasrecebidasController@relatoriojaneiro')->name('pdf');

//Relatorio Fevereiro
Route::get('relatoriofevereirorecebido', 'PdfContasrecebidasController@relatoriofevereiro')->name('pdf');

//Relatorio Março
Route::get('relatoriomarcorecebido', 'PdfContasrecebidasController@relatoriomarco')->name('pdf');

//Relatorio Abril
Route::get('relatorioabrilrecebido', 'PdfContasrecebidasController@relatorioabril')->name('pdf');

//Relatorio Maio
Route::get('relatoriomaiorecebido', 'PdfContasrecebidasController@relatoriomaio')->name('pdf');

//Relatorio Junho
Route::get('relatoriojunhorecebido', 'PdfContasrecebidasController@relatoriojunho')->name('pdf');

//Relatorio Julho
Route::get('relatoriojulhorecebido', 'PdfContasrecebidasController@relatoriojulho')->name('pdf');

//Relatorio Agosto
Route::get('relatorioagostorecebido', 'PdfContasrecebidasController@relatorioagosto')->name('pdf');

//Relatorio Setembro
Route::get('relatoriosetembrorecebido', 'PdfContasrecebidasController@relatoriosetembro')->name('pdf');

//Relatorio Outubro
Route::get('relatoriooutubrorecebido', 'PdfContasrecebidasController@relatoriooutubro')->name('pdf');

//Relatorio Novembro
Route::get('relatorionovembrorecebido', 'PdfContasrecebidasController@relatorionovembro')->name('pdf');

//Relatorio Dezembro
Route::get('relatoriodezembrorecebido', 'PdfContasrecebidasController@relatoriodezembro')->name('pdf');







Auth::routes(['verify' => true]);

Route::get('home', 'HomeController@index')->middleware('verified');

Route::resource('empresas', 'empresasController');

Route::resource('clientes', 'clientesController');

Route::resource('contasapagars', 'contasapagarController');

Route::resource('contasarecebers', 'contasareceberController');

Route::resource('users', 'usersController');

Route::resource('professors', 'professorsController');

Route::resource('cursos', 'cursosController');

Route::resource('blogs', 'blogsController');

Route::resource('banners', 'bannersController');

Route::resource('leads', 'leadsController');

Route::resource('alunos', 'alunoController');

Route::resource('depoimentos', 'depoimentosController');