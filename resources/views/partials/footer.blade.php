  <!--FOOER AREA-->
  <footer class="footer-area sky-gray-bg relative">
        <div class="footer-top-area padding-80-80 bg-dark">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12">
                        <div class="single-footer footer-about sm-mb50 xs-mb50 sm-center xs-center">
                            <div class="footer-logo mb30">
                                <a href="#"><img src="img/logo.png" alt="" style="width:50%"></a>
                            </div>
                            <p class="text-justify"><b>Edtech para profissionais e apaixonados por tecnologia</b>. 
<br>
Nossa missão é a acompanhar o profissional de tecnologia de ponta a ponta, desde a  qualificação profissional ao encaminhamento para o mercado de trabalho, assim formando um ecossistema de profissionais da tecnologia preparados para o futuro.
</p>
                        </div>
                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-4 col-xs-12">
                        <div class="single-footer footer-list white xs-center xs-mb50">
                        <a href="http://bit.ly/clubetech">
                            <ul>
                                <li><i class="fa fa-whatsapp fa-4x" aria-hidden="true"></i></li>
                                <li>Fale com a gente !</li>
                                
                            </ul>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-5 col-lg-5 col-sm-8 col-xs-12">
                        <div class="single-footer footer-subscribe white xs-center">
                            <h3 class="mb30 xs-font18">Não perca a oportunidade</h3>
                            <p>Matricule-se em um curso e se destaque no mercado de trabalho.</p>
                      </section>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom-area white">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="footer-copyright text-center wow fadeIn">
                            
Copyright &copy;<script>document.write(new Date().getFullYear());</script> | Somos apaixonado por tecnologia e VOCÊ ?  |  Criado </i>  por<a href="http://temet.com.br/" target="_blank"> Temet Desenvolvimento</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--FOOER AREA END-->