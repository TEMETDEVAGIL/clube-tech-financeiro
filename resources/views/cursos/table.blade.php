<div class="table-responsive">
    <table class="table" id="cursos-table">
        <thead>
            <tr>
                <th>Titulo</th>
        <th>Data</th>
        <th>Carga horária</th>
        <th>Valor</th>
        <th>Professor Id</th>
        <th>Status</th>
                <th colspan="3">Ações</th>
            </tr>
        </thead>
        <tbody>
        @foreach($cursos as $cursos)
            <tr>
                <td>{{ $cursos->titulo }}</td>
           
            <td>{{ $cursos->data }}</td>
            <td>{{ $cursos->cargahoraria }}</td>
            <td>{{ $cursos->valor }}</td>
            <td>{{ $cursos->professor_id }}</td>
            <td>{{ $cursos->status }}</td>
                <td>
                    {!! Form::open(['route' => ['cursos.destroy', $cursos->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('cursos.show', [$cursos->id]) }}" class='btn btn-success btn-xs'><b>Verificar</b></a>
                        <a href="{{ route('cursos.edit', [$cursos->id]) }}" class='btn btn-danger btn-xs'><b>Editar</b></a>
                        <a href="uploadCurso" class='btn btn-primary btn-xs'><b>Imagem</b></a>

                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
