<!-- Titulo Field -->
<div class="form-group">
    {!! Form::label('titulo', 'Titulo:') !!}
    <p>{{ $cursos->titulo }}</p>
</div>

<!-- Fraseefeito Field -->
<div class="form-group">
    {!! Form::label('fraseefeito', 'Fraseefeito:') !!}
    <p>{{ $cursos->fraseefeito }}</p>
</div>

<!-- Minidescricao Field -->
<div class="form-group">
    {!! Form::label('minidescricao', 'Minidescricao:') !!}
    <p>{{ $cursos->minidescricao }}</p>
</div>

<!-- Descricao Field -->
<div class="form-group">
    {!! Form::label('descricao', 'Descricao:') !!}
    <p>{{ $cursos->descricao }}</p>
</div>

<!-- Data Field -->
<div class="form-group">
    {!! Form::label('data', 'Data:') !!}
    <p>{{ $cursos->data }}</p>
</div>

<!-- Cargahoraria Field -->
<div class="form-group">
    {!! Form::label('cargahoraria', 'Cargahoraria:') !!}
    <p>{{ $cursos->cargahoraria }}</p>
</div>

<!-- Conteudo Field -->
<div class="form-group">
    {!! Form::label('conteudo', 'Conteudo:') !!}
    <p>{{ $cursos->conteudo }}</p>
</div>

<!-- Requisitos Field -->
<div class="form-group">
    {!! Form::label('requisitos', 'Requisitos:') !!}
    <p>{{ $cursos->requisitos }}</p>
</div>

<!-- Publico Field -->
<div class="form-group">
    {!! Form::label('publico', 'Publico:') !!}
    <p>{{ $cursos->publico }}</p>
</div>

<!-- Modulo1 Field -->
<div class="form-group">
    {!! Form::label('modulo1', 'Modulo1:') !!}
    <p>{{ $cursos->modulo1 }}</p>
</div>

<!-- Modulo2 Field -->
<div class="form-group">
    {!! Form::label('modulo2', 'Modulo2:') !!}
    <p>{{ $cursos->modulo2 }}</p>
</div>

<!-- Modulo3 Field -->
<div class="form-group">
    {!! Form::label('modulo3', 'Modulo3:') !!}
    <p>{{ $cursos->modulo3 }}</p>
</div>

<!-- Modulo4 Field -->
<div class="form-group">
    {!! Form::label('modulo4', 'Modulo4:') !!}
    <p>{{ $cursos->modulo4 }}</p>
</div>

<!-- Modulo5 Field -->
<div class="form-group">
    {!! Form::label('modulo5', 'Modulo5:') !!}
    <p>{{ $cursos->modulo5 }}</p>
</div>

<!-- Modulo6 Field -->
<div class="form-group">
    {!! Form::label('modulo6', 'Modulo6:') !!}
    <p>{{ $cursos->modulo6 }}</p>
</div>

<!-- Modulo7 Field -->
<div class="form-group">
    {!! Form::label('modulo7', 'Modulo7:') !!}
    <p>{{ $cursos->modulo7 }}</p>
</div>

<!-- Modulo8 Field -->
<div class="form-group">
    {!! Form::label('modulo8', 'Modulo8:') !!}
    <p>{{ $cursos->modulo8 }}</p>
</div>

<!-- Modulo9 Field -->
<div class="form-group">
    {!! Form::label('modulo9', 'Modulo9:') !!}
    <p>{{ $cursos->modulo9 }}</p>
</div>

<!-- Modulo10 Field -->
<div class="form-group">
    {!! Form::label('modulo10', 'Modulo10:') !!}
    <p>{{ $cursos->modulo10 }}</p>
</div>

<!-- Valor Field -->
<div class="form-group">
    {!! Form::label('valor', 'Valor:') !!}
    <p>{{ $cursos->valor }}</p>
</div>

<!-- Professor Id Field -->
<div class="form-group">
    {!! Form::label('professor_id', 'Professor Id:') !!}
    <p>{{ $cursos->professor_id }}</p>
</div>

<!-- Link Field -->
<div class="form-group">
    {!! Form::label('link', 'Link:') !!}
    <p>{{ $cursos->link }}</p>
</div>

<!-- Imagem Field -->
<div class="form-group">
    {!! Form::label('imagem', 'Imagem:') !!}
    <p>{{ $cursos->imagem }}</p>
</div>

<!-- Banner Field -->
<div class="form-group">
    {!! Form::label('banner', 'Banner:') !!}
    <p>{{ $cursos->banner }}</p>
</div>

<!-- Contadormes Field -->
<div class="form-group">
    {!! Form::label('contadormes', 'Contadormes:') !!}
    <p>{{ $cursos->contadormes }}</p>
</div>

<!-- Contadordia Field -->
<div class="form-group">
    {!! Form::label('contadordia', 'Contadordia:') !!}
    <p>{{ $cursos->contadordia }}</p>
</div>

<!-- Contadorano Field -->
<div class="form-group">
    {!! Form::label('contadorano', 'Contadorano:') !!}
    <p>{{ $cursos->contadorano }}</p>
</div>

<!-- Contadorhorario Field -->
<div class="form-group">
    {!! Form::label('contadorhorario', 'Contadorhorario:') !!}
    <p>{{ $cursos->contadorhorario }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $cursos->status }}</p>
</div>

