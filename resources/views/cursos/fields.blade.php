<!-- Titulo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('titulo', 'Titulo:') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<!-- Fraseefeito Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fraseefeito', 'Fraseefeito:') !!}
    {!! Form::text('fraseefeito', null, ['class' => 'form-control']) !!}
</div>

<!-- Minidescricao Field -->
<div class="form-group col-sm-6">
    {!! Form::label('minidescricao', 'Minidescricao:') !!}
    {!! Form::textarea('minidescricao', null, ['class' => 'form-control']) !!}
</div>

<!-- Descricao Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descricao', 'Descricao:') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control']) !!}
</div>

<!-- Data Field -->
<div class="form-group col-sm-6">
    {!! Form::label('data', 'Data:') !!}
    {!! Form::text('data', null, ['class' => 'form-control']) !!}
</div>

<!-- Cargahoraria Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cargahoraria', 'Cargahoraria:') !!}
    {!! Form::text('cargahoraria', null, ['class' => 'form-control']) !!}
</div>

<!-- Conteudo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('conteudo', 'Conteudo:') !!}
    {!! Form::textarea('conteudo', null, ['class' => 'form-control']) !!}
</div>

<!-- Requisitos Field -->
<div class="form-group col-sm-6">
    {!! Form::label('requisitos', 'Requisitos:') !!}
    {!! Form::textarea('requisitos', null, ['class' => 'form-control']) !!}
</div>

<!-- Publico Field -->
<div class="form-group col-sm-6">
    {!! Form::label('publico', 'Publico:') !!}
    {!! Form::textarea('publico', null, ['class' => 'form-control']) !!}
</div>

<!-- Modulo1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modulo1', 'Modulo1:') !!}
    {!! Form::textarea('modulo1', null, ['class' => 'form-control']) !!}
</div>

<!-- Modulo2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modulo2', 'Modulo2:') !!}
    {!! Form::textarea('modulo2', null, ['class' => 'form-control']) !!}
</div>

<!-- Modulo3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modulo3', 'Modulo3:') !!}
    {!! Form::textarea('modulo3', null, ['class' => 'form-control']) !!}
</div>

<!-- Modulo4 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modulo4', 'Modulo4:') !!}
    {!! Form::textarea('modulo4', null, ['class' => 'form-control']) !!}
</div>

<!-- Modulo5 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modulo5', 'Modulo5:') !!}
    {!! Form::textarea('modulo5', null, ['class' => 'form-control']) !!}
</div>

<!-- Modulo6 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modulo6', 'Modulo6:') !!}
    {!! Form::textarea('modulo6', null, ['class' => 'form-control']) !!}
</div>

<!-- Modulo7 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modulo7', 'Modulo7:') !!}
    {!! Form::textarea('modulo7', null, ['class' => 'form-control']) !!}
</div>

<!-- Modulo8 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modulo8', 'Modulo8:') !!}
    {!! Form::textarea('modulo8', null, ['class' => 'form-control']) !!}
</div>

<!-- Modulo9 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modulo9', 'Modulo9:') !!}
    {!! Form::textarea('modulo9', null, ['class' => 'form-control']) !!}
</div>

<!-- Modulo10 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modulo10', 'Modulo10:') !!}
    {!! Form::textarea('modulo10', null, ['class' => 'form-control']) !!}
</div>

<!-- Valor Field -->
<div class="form-group col-sm-6">
    {!! Form::label('valor', 'Valor:') !!}
    {!! Form::text('valor', null, ['class' => 'form-control']) !!}
</div>

<!-- Professor Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('professor_id', 'Professor Id:') !!}
    {!! Form::number('professor_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Link Field -->
<div class="form-group col-sm-6">
    {!! Form::label('link', 'Link Sympla:') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<!-- Imagem Field -->
<div class="form-group col-sm-6">
    {!! Form::label('imagem', 'Imagem:') !!}
    {!! Form::text('imagem', null, ['class' => 'form-control']) !!}
</div>

<!-- Banner Field -->
<div class="form-group col-sm-6">
    {!! Form::label('banner', 'Banner:') !!}
    {!! Form::text('banner', null, ['class' => 'form-control']) !!}
</div>

<!-- Contadormes Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contadormes', 'Contador Mês:') !!}
    {!! Form::text('contadormes', null, ['class' => 'form-control', 'placeholder'=>'00']) !!}
</div>

<!-- Contadordia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contadordia', 'Contador Dia:') !!}
    {!! Form::text('contadordia', null, ['class' => 'form-control', 'placeholder'=>'00']) !!}
</div>

<!-- Contadorano Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contadorano', 'Contador Ano:') !!}
    {!! Form::text('contadorano', null, ['class' => 'form-control', 'placeholder'=>'0000']) !!}
</div>

<!-- Contadorhorario Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contadorhorario', 'Contador horário:') !!}
    {!! Form::text('contadorhorario', null, ['class' => 'form-control', 'placeholder'=>'00:00']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('status', 0) !!}
        {!! Form::checkbox('status', '1', null) !!}
    </label>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('cursos.index') }}" class="btn btn-default">Cancel</a>
</div>
