@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">            
        <b>Relatórios Contas Recebidas 2020</b>
</h1>
        
    </section>
    <div class="content">
        <div class="clearfix"></div>
        <div class="box box-primary">  
        </div>

        <div class="container">
    <div class="row">

    <div class="row" style="margin-top:2%">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>Janeiro</h3>

             <a href="relatoriojaneirorecebido" ><button class="btn btn-primary"> Gerar Relatório</button></a>

            </div>
           
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3> Fevereiro</h3>

            <a href="relatoriofevereirorecebido" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
           
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Março</h3>

            <a href="relatoriomarcorecebido" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Abril</h3>

            <a href="relatorioabrilrecebido" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
            
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Maio</h3>

            <a href="relatoriomaiorecebido" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
            
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Junho</h3>

            <a href="relatoriojunhorecebido" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
            
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Julho</h3>

            <a href="relatoriojulhorecebido" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
            
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Agosto</h3>

            <a href="relatorioagostorecebido" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
            
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Setembro</h3>

            <a href="relatoriosetembrorecebido" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
            
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Outubro</h3>

            <a href="relatoriooutubrorecebido" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
            
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Novembro</h3>

            <a href="relatorionovembrorecebido" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
            
          </div>
        </div>


        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Dezembro</h3>

            <a href="relatoriodezembrorecebido" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
            
          </div>
        </div>
       
      </div>
    </div>
</div>
        
       
    </div>
@endsection

 