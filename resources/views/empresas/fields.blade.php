<!-- Razaosocial Field -->
<div class="form-group col-sm-6">
    {!! Form::label('razaosocial', 'Razaosocial:') !!}
    {!! Form::text('razaosocial', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-success']) !!}
    <a href="{{ route('empresas.index') }}" class="btn btn-danger">Cancelar</a>
</div>
