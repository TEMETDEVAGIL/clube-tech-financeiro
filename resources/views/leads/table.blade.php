<div class="table-responsive">
    <table class="table" id="leads-table">
        <thead>
            <tr>
                <th>Nome</th>
        <th>Email</th>
        <th>Telefone</th>
        <th>Assunto</th>
                <th colspan="3">Ações</th>
            </tr>
        </thead>
        <tbody>
        @foreach($leads as $leads)
            <tr>
                <td>{{ $leads->nome }}</td>
            <td>{{ $leads->email }}</td>
            <td>{{ $leads->telefone }}</td>
            <td>{{ $leads->assunto }}</td>
                <td>
                    {!! Form::open(['route' => ['leads.destroy', $leads->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('leads.show', [$leads->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('leads.edit', [$leads->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
