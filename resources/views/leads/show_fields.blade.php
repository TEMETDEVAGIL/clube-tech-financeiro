<!-- Nome Field -->
<div class="form-group">
    {!! Form::label('nome', 'Nome:') !!}
    <p>{{ $leads->nome }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $leads->email }}</p>
</div>

<!-- Telefone Field -->
<div class="form-group">
    {!! Form::label('telefone', 'Telefone:') !!}
    <p>{{ $leads->telefone }}</p>
</div>

<!-- Assunto Field -->
<div class="form-group">
    {!! Form::label('assunto', 'Assunto:') !!}
    <p>{{ $leads->assunto }}</p>
</div>

<!-- Mensagem Field -->
<div class="form-group">
    {!! Form::label('mensagem', 'Mensagem:') !!}
    <p>{{ $leads->mensagem }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $leads->status }}</p>
</div>

