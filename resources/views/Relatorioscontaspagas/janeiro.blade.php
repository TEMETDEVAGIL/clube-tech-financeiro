<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Clube Tech | Relatório janeiro</title>
    </head>
    <body >

  <center> 
   <div class="card mb-3">
  <img class="card-img-top" src="img/logo2.png" alt="Card image" style="width:30%">
  <div class="card-body">
    <h3 class="card-title">Clube = agência = TECNOLOGIA = inovação = você</h3>
  </div>
</div>
</center>

<div class="card" style="margin-top:10%">
  <div class="card-body">
    <b style="font-size:20px"> Relatório de contas pagas Janeiro 2020</b>
  </div>
</div>
     
<center>
<div class="table-responsive" style="margin-top:5%">
    <table class="table" id="leads-table" style="background-color:white">
        <thead>
            <tr>
         <th style="font-size:20px"><b>Empresa | </b></th>
         <th style="font-size:20px">Data de Vencimento | </th>
        <th style="font-size:20px">Data de pagamento | </th>
        <th style="font-size:20px">Valor</th>
            </tr>
        </thead>
        <tbody>
        @foreach($relatoriojaneiro as $janeiro)
            <tr>
                <td>{!! $janeiro->empresa_id !!}</td>
                <td>{!! $janeiro->datavencimento !!}</td>
                <td>{!! $janeiro->datapagamento !!}</td>
                <td>{!! $janeiro->valor !!}</td> 
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<br>

<div class="card">
  <div class="card-header">
    <b>Total</b>: {!! $totaljaneiro !!}
  </div>

</div>
</center>

 
    </body>
</html>