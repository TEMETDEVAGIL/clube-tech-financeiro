<div class="table-responsive">
    <table class="table" id="banners-table">
        <thead>
            <tr>
                <th>Titulo</th>
               <th>Imagem</th>
               <th>Upload de Imagem</th>

                <th colspan="3">Ações</th>
            </tr>
        </thead>
        <tbody>
        @foreach($banners as $banners)
            <tr>
                <td>{{ $banners->titulo }}</td>
            <td>{{ $banners->imagem }}</td>
            <td>  
                <div class='btn-group'>
                    <a href="uploadBanner" class='btn btn-success btn-xs'>Adicione uma imagem <i class="glyphicon glyphicon-picture"></i></a>
                </div>
                </td>
                <td>
                    {!! Form::open(['route' => ['banners.destroy', $banners->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('banners.edit', [$banners->id]) }}" class='btn btn-primary btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
