<!-- Titulo Field -->
<div class="form-group">
    {!! Form::label('titulo', 'Titulo:') !!}
    <p>{{ $banners->titulo }}</p>
</div>

<!-- Imagem Field -->
<div class="form-group">
    {!! Form::label('imagem', 'Imagem:') !!}
    <p>{{ $banners->imagem }}</p>
</div>

