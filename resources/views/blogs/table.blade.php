<div class="table-responsive">
    <table class="table" id="blogs-table">
        <thead>
            <tr>
                <th>Titulo</th>
        <th>Imagem</th>
        <th>Data</th>
        <th>Link</th>
        <th>Upload de Imagem</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($blogs as $blogs)
            <tr>
                <td>{{ $blogs->titulo }}</td>
            <td>{{ $blogs->imagem }}</td>
            <td>{{ $blogs->data }}</td>
            <td>{{ $blogs->link }}</td>
            <td>  
                <div class='btn-group'>
                    <a href="uploadBlog" class='btn btn-success btn-xs'>Adicione uma imagem <i class="glyphicon glyphicon-picture"></i></a>
                </div>
                </td>
            <td>{{ $blogs->status }}</td>
                <td>
                    {!! Form::open(['route' => ['blogs.destroy', $blogs->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('blogs.show', [$blogs->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('blogs.edit', [$blogs->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
