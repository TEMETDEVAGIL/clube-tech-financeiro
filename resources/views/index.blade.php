<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <!--====== USEFULL META ======-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Alinhamos as experiências profissionais com métodos educacionais para obter uma metodologia de aula que tenha foco na qualificação dos alunos de forma integral." />
    <meta name="keywords" content="Curso, Tecnologia, online" />

    <!--====== TITLE TAG ======-->
    <title>Clube Tech | Sua escola de tecnologia</title>

    <!--====== FAVICON ICON =======-->
    <link rel="shortcut icon" type="image/ico" href="img/favicon.png" />

    <!--====== STYLESHEETS ======-->
    <link href="css/plugins.css" rel="stylesheet">
    <link href="css/theme.css" rel="stylesheet">
    <link href="css/icons.css" rel="stylesheet">

    <!--====== MAIN STYLESHEETS ======-->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <script src="js/vendor/modernizr-2.8.3.min.js"></script>

    <!-- ====== Modal POP UP =================-->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>


<!--============ chosen ========== -->
<link href="http://harvesthq.github.io/chosen/chosen.css" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<script src="http://harvesthq.github.io/chosen/chosen.jquery.js"></script>

<!--============ SWAL ========== -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '602826070441240');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=602826070441240&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->




  
</head>

 <!--POP UP
 <?php
			$situacao_usuario = "pendente";
			if($situacao_usuario == "pendente"){ ?>
				<script>
					$(document).ready(function(){
						$('#myModal').modal('show');
					});
				</script>
            <?php } ?>
                -->
          
            
            

<body data-spy="scroll" data-target=".mainmenu-area" data-offset="90">

<!-- Modal pop up -->
<div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:3%">
		<div class="modal-dialog " role="document" style="background-color:#63c8d0">
          <div class="modal-content">
            <div class="modal-header" style="background-color:white">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:red"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" style="background-color:#63c8d0">
			<div class="container-fluid">
    <div class="row">
        <div class="col-12 mt-3">
            <div class="">
                <div class="card-horizontal" >
                    <div class="img-square-wrapper">
                        <img class="img-popup" src="img/logo.png" alt="Card image cap" style="width:30%">
                    </div>
                    <br>
                    <div class="card-body pose-popup" >
                        <h4 class="card-title" style="font-size:20px;color:white">Uma nova forma de aprender Tecnologia !</h4><br>
                        <h4 class="card-title" style="font-size:20px;color:white">Agende uma visita <br>(71) 99377-1555</h4>
						
      </div>
     </div>
                </div>  
            </div>
        </div>
    </div>
</div>          
 </div>
   </div>
</div>


<!-- Modal Comentario -->
<div class="modal fade " id="modalcomentario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:3%">
		<div class="modal-dialog " role="document" style="background-color:#63c8d0">
          <div class="modal-content">
            <div class="modal-header" style="background-color:white">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:red"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" >
            {!! Form:: open(['route'=>'depoimento.save', 'method' => 'POST','id' => 'login-form','style' => 'display:block']) !!}                               
            <div class="form-group">
                <label for="exampleFormControlSelect1">Nome Completo </label>
			    <input type="text" name="nome" id="nome" tabindex="1" class="form-control" required >
                </div>
                
            <div class="form-group">
                <label for="exampleFormControlSelect1">Email</label>
			    <input type="email" name="email" id="email" tabindex="1" class="form-control" required >
				</div>
              <div class="form-group">
                <label for="exampleFormControlSelect1">Escolha o curso</label>
                 <select name="curso" id="curso" class="form-control">
                   @foreach ($cursos as $cursos)
                     <option name="cursos" value="{{ $cursos->id }}" >{{ $cursos->titulo }}</option>
                     @endforeach
                    </select>
                    </div>

                    <div class="form-group estrelas">
                    <p>Avaliação do curso</p>
                                <input type="radio" id="cm_star-empty" name="nota" value="" checked/>
                                <label for="cm_star-1"><i class="fa"></i></label>
                                <input type="radio" id="cm_star-1" name="nota" value="1"/>
                                <label for="cm_star-2"><i class="fa"></i></label>
                                <input type="radio" id="cm_star-2" name="nota" value="2"/>
                                <label for="cm_star-3"><i class="fa"></i></label>
                                <input type="radio" id="cm_star-3" name="nota" value="3"/>
                                <label for="cm_star-4"><i class="fa"></i></label>
                                <input type="radio" id="cm_star-4" name="nota" value="4"/>
                                <label for="cm_star-5"><i class="fa"></i></label>
                                <input type="radio" id="cm_star-5" name="nota" value="5"/>
                                <label for="cm_star-6"><i class="fa"></i></label>
                                <input type="radio" id="cm_star-6" name="nota" value="6"/>
                                <label for="cm_star-7"><i class="fa"></i></label>
                                <input type="radio" id="cm_star-7" name="nota" value="7"/>
                                <label for="cm_star-8"><i class="fa"></i></label>
                                <input type="radio" id="cm_star-8" name="nota" value="8"/>
                                <label for="cm_star-9"><i class="fa"></i></label>
                                <input type="radio" id="cm_star-9" name="nota" value="9"/>
                                <label for="cm_star-10"><i class="fa"></i></label>
                                <input type="radio" id="cm_star-10" name="nota" value="10"/>
                                </div>
                      </div>

                

                    <div class="form-group">
                    <label for="exampleFormControlTextarea1">Comentário</label>
                     <textarea class="form-control" id="comentario" name="comentario" rows="3" required></textarea>
                      </div>
									
				<div class="form-group">
				  <div class="row">
				  <div class="col-sm-6 col-sm-offset-3">
					<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="btn btn-primary col-md-12" value="Finalizar">
					</div>
					</div>
					</div>
									
					</form>                          

           	
            </div>          
            </div>
            </div>
            </div>
 <!-- Modal Professor -->
<div class="modal fade" id="modalprofessor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
 
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

  {!! Form:: open(['route'=>'inscricao.save', 'method' => 'POST']) !!}

  <div class="tab">
  <h1>Quem é você?</h1>
  <br>
  <div class="row">
    <div class="col-md-6">
    <label class="control-label">Nome Completo</label>
    <input type="text" class="form-control"  id="nome" name="nome" required/> 
    </div>

    <div class="col-md-6">
    <label class="control-label">CPF</label>
    <input type="text" class="form-control"  id="cpf" name="cpf" required />
    </div>

  </div>

  <div class="row">
    <div class="col-md-6">
    <label class="control-label">Telefone</label>
    <input type="text" class="form-control"  id="telefone" name="telefone" required />  
    </div>

    <div class="col-md-6">
    <label class="control-label">Email</label>
     <input type="text" class="form-control"  id="email" name="email" required />
    </div>
  </div>
  <br>

  <div class="row">
    <div class="col-md-12">
    <label class="control-label">Fale um pouco sobre você</label>
    <textarea class="form-control" id="minicv" name="minicv" rows="4" required></textarea> 
    </div>

  </div>
 
  </div>

  <div class="tab">
  <h1>Onde reside ?</h1>
  <br>
  <div class="row">
    <div class="col-md-4">
    <label class="control-label">CEP</label>
     <input type="text" class="form-control"  id="cep" name="cep" placeholder="Insira o CEP" required /> 
    </div>

    <div class="col-md-4">
    <label class="control-label">Logradouro</label>
     <input type="text" class="form-control"  id="logradouro" name="logradouro" readonly />
    </div>

    <div class="col-md-4">
    <label class="control-label">Bairro</label>
    <input type="text" class="form-control"  id="bairro" name="bairro" readonly /> 
    </div>

  </div>

  <div class="row">
    <div class="col-md-4">
    <label class="control-label">Cidade</label>
     <input type="text" class="form-control"  id="cidade" name="cidade" readonly />  
    </div>

    <div class="col-md-4">
    <label class="control-label">UF</label>
    <input type="text" class="form-control"  id="uf" name="uf" readonly/>
    </div>

    <div class="col-md-4">
    <label class="control-label">Complemento</label>
    <input  class="form-control"  id="complemento" name="complemento" required>  </input>
    </div>
  </div>

  </div>

 
  <div style="margin-top:5%">
  <div style="overflow:auto;" >
    <div style="float:right;">
      <button type="button" id="prevBtn" onclick="nextPrev(-1)">Voltar</button>
      <button type="button" id="nextBtn" onclick="nextPrev(1)">Voltar</button>
      <button type="submit" id="submit" ></button>
    </div>
  </div>
  </div>

  <!-- Circles which indicates the steps of the form: -->
  <div style="text-align:center;margin-top:40px;">
    <span class="step"></span>
  </div>
  {!!Form:: close()!!} 
      </div>
    
    </div>
  </div>
</div>

<!-- Modal Comentarios -->
<div class="modal fade" id="modalcomentario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>




    <!--- PRELOADER -->
    <div class="preeloader">
        <div class="preloader-spinner"></div>
    </div>

    

    <!--START TOP AREA-->

    @if(Session::has('fail'))
    <div class="alert alert-warning">
       {{Session::get('fail')}}
    </div>
@endif

  
    <header class="top-area" id="home">
        <div class="header-top-area" id="scroolup">
            <!--MAINMENU AREA-->
            <div class="mainmenu-area" id="mainmenu-area">
                <div class="mainmenu-area-bg"></div>
                <nav class="navbar">
                    <div class="container">
                        <div class="navbar-header">
                            <a href="#home" class="navbar-brand"><img src="img/logo.png" alt="logo" style="width:19%"></a>
                        </div>
                        <div id="main-nav" class="stellarnav">
                        <ul id="nav" class="nav navbar-nav pull-right">
                                <li class="active"><a href="#home" style="color:white">Home</a></li>
                                <li><a href="#features">Metodologia</a></li>
                                <li><a href="#courses">Cursos</a></li>
                                <li><a href="#testmonial">Depoimentos</a></li>
                                <li><a href="#blog">Blog</a></li>
                                <li><a href="#faqs">Faq</a></li>
                                <li><a href="#contact">Contato</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <!--END MAINMENU AREA END-->
        </div>

        <!--WELCOME SLIDER AREA-->
        <div class="welcome-slider-area white font16">
            <div class="welcome-single-slide">
            @foreach ($banner1 as $banner1)
                <div class="slide-bg-overlay" style="background: rgba(0, 0, 0, 0) url('uploads/banners/{{$banner1->imagem}}') no-repeat scroll center center / cover;"></div>
                @endforeach

                <div class="welcome-area">
                    <div class="container">
                        <div class="row flex-v-center">
                            <div class="col-md-8 col-lg-7 col-sm-12 col-xs-12">
                                <div class="welcome-text">
                                    <h1> VEM PRO CLUBE ! </h1>
                                    <p>Was certainty remaining engrossed applauded sir how discovery. Settled opinion how enjoyed greater joy adapted too shy. Now properly surprise expenses.</p>
                                    <div class="home-button">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="welcome-single-slide">
            @foreach ($banner2 as $banner2)
                <div class="slide-bg-overlay" style="background: rgba(0, 0, 0, 0) url('uploads/banners/{{$banner2->imagem}}') no-repeat scroll center center / cover;"></div>
                @endforeach

                <div class="welcome-area">
                    <div class="container">
                        <div class="row flex-v-center">
                            <div class="col-md-8 col-lg-7 col-sm-12 col-xs-12">
                                <div class="welcome-text">
                                    <h1> VEM PRO CLUBE ! </h1>
                                    <p>Was certainty remaining engrossed applauded sir how discovery. Settled opinion how enjoyed greater joy adapted too shy. Now properly surprise expenses.</p>
                                    <div class="home-button">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="welcome-single-slide">
            @foreach ($banner3 as $banner3)
                <div class="slide-bg-overlay" style="background: rgba(0, 0, 0, 0) url('uploads/banners/{{$banner3->imagem}}') no-repeat scroll center center / cover;"></div>
                @endforeach
                <div class="welcome-area">
                    <div class="container">
                        <div class="row flex-v-center">
                            <div class="col-md-8 col-lg-7 col-sm-12 col-xs-12">
                                <div class="welcome-text">
                                    <h1> Clube = agência = TECNOLOGIA = inovação = você </h1>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          
        </div>
        <!--WELCOME SLIDER AREA END-->
    </header>
    <!--END TOP AREA-->

    <!--FEATURES TOP AREA-->
    <section class="features-top-area" id="features">
        <div class="container">
            <div class="row promo-content">
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box mb20 xs-mb0 wow fadeInUp padding30" data-wow-delay="0.1s">
                        <div class="box-icon features-box-icon">
                            <i class="fa fa-graduation-cap" ></i>
                        </div>
                        <h3 class="box-title">Cursos</h3>
                        <p class="text-justify">Sua qualificação profissional, com qualidade e eficiência. Cursos, treinamentos, workshops e palestras para o mercado tech.</p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb20 xs-mb0  wow fadeInUp padding30" data-wow-delay="0.2s">
                        <div class="box-icon features-box-icon">
                            <i class="icofont icofont-business-man-alt-1"></i>
                        </div>
                        <h3 class="box-title">Consultoria</h3>
                        <p class="text-justify">Profissionais que tem o propósito de levantar as necessidades dos alunos, identificar soluções e recomendar ações de um determinado problema.</p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb20 xs-mb0 wow fadeInUp padding30" data-wow-delay="0.3s">
                        <div class="box-icon features-box-icon">
                            <i class="fa fa-rocket"></i>
                        </div>
                        <h3 class="box-title">Coworking</h3>
                        <p class="text-justify"> Espaço colaborativo. Seu escritório virtual e presencial.</p>
                    </div>
                </div>
               
            </div>
        </div>
    </section>
    <!--FEATURES TOP AREA END-->

    <!--ABOUT TOP CONTENT AREA-->
    <section class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="text-center wow fadeIn">
                    <h2 class="xs-font20" style="color:white;background-color:#292929">Profissionais + Alunos + Tecnologia + Ensino.</h2>
                        <p class="text-justify">Alinhamos as experiências profissionais com métodos educacionais para obter uma metodologia de aula que tenha foco na
                        qualificação dos alunos de forma integral.</p>
                        <p class="text-justify"> Em nossos cursos além das aulas oferecemos <b>exercícios práticos e teóricos, palestras com profissionais convidados, 
                        sessões de coaching</b> entre outros métodos, assim agregando ainda mais para o seu futuro profissional.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--ABOUT TOP CONTENT AREA END-->

     <!--FUN FACT AREA AREA-->
     @foreach ($banner4 as $banner4)
     <section class=" center white relative padding-100-70" id="fact" style=" background: rgba(0, 0, 0, 0) url('uploads/banners/{{$banner4->imagem}}') no-repeat scroll center center / cover;">
     @endforeach

        <div class="area-bg" data-stellar-background-ratio="0.6"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="single-fun-fact mb30 wow fadeInUp" data-wow-delay="0.1s">
                        <h3 class="font60 xs-font26"><span class="counter">200</span>+</h3>
                        <p class="font600">Alunos Qualificados</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="single-fun-fact mb30 wow fadeInUp" data-wow-delay="0.2s">
                        <h3 class="font60 xs-font26"><span class="counter">50</span>+</h3>
                        <p class="font600">Cursos Ofertados</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="single-fun-fact mb30 wow fadeInUp" data-wow-delay="0.3s">
                        <h3 class="font60 xs-font26"><span class="counter">5</span>+</h3>
                        <p class="font600">Empresas Parceiras</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="single-fun-fact mb30 wow fadeInUp" data-wow-delay="0.4s">
                        <h3 class="font60 xs-font26"><span class="counter">70</span>+</h3>
                        <p class="font600">Profissionais Inseridos no mercado</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--FUN FACT AREA AREA END-->

    <!--ABOUT AREA-->
    <section class="section-padding about-area" id="about">
        <div class="container">
            <div class="row flex-v-center">
                <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12">
                    <div class="about-content xs-mb50 wow fadeIn">
                        <h1 class="xs-font20 mb30">Espaço Colaborativo</h1>
                        <p class="text-justify">Um movimento de pessoas, empresas e comunidades que buscam trabalhar e desenvolver suas vidas e negócios juntos, para crescer de forma mais rápida e colaborativa.</p>
                        <p class="text-justify" style="font-size:20px;color:#63c8d0"><b>Venha conhecer !</b> </p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-md-offset-1 col-lg-offset-1 col-sm-12 col-xs-12">
                    <div class="video-promo-details wow fadeIn">
                        <div class="video-promo-content">
                            <span data-video-id="uvr9Fj7VBew" class="video-area-popup mb30"><i class="fa fa-play"></i></span>
                        </div>
                        <img src="img/about/coworking.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--ABOUT AREA END-->

    <!--COURSE AREA-->
    <section class="course-area padding-top" id="courses" style="margin-top:-5%">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2 class="xs-font26">Cursos Disponíveis</h2>
                    </div>
                </div>
            </div>
            

            <div class="row course-list">
            @foreach ($curso1 as $curso1)
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-course mb20">
                        <img src="uploads/cursos/{{$curso1->imagem}}" alt="">
                        <div class="course-details padding30">
                            <h3 class="font18">{{$curso1->titulo}}</h3>
                            <p>{{$curso1->minidescricao}}</p>

                            {!! Form:: open(['route'=>'curso.curso', 'method' => 'POST']) !!}
                            <input value="{{$curso1->id}}" id="cursoid" name="cursoid" readonly style="display:none">
                            <input value="{{$curso1->professor_id}}" id="professorid" name="professorid" readonly style="display:none">
                            <p class="mt30"><button class="btn btn-primary" type="submit">Ver mais infomações</button> <span class="course-price">R$ {{$curso1->valor}}</span></p>
                            {!!Form:: close()!!}                      

                        </div>
                    </div>
                </div>
                @endforeach
                @foreach ($curso2 as $curso2)
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-course mb20">
                        <img src="uploads/cursos/{{$curso2->imagem}}" alt="">
                        <div class="course-details padding30">
                            <h3 class="font18">{{$curso2->titulo}}</h3>
                            <p>{{$curso2->minidescricao}}</p>
                            {!! Form:: open(['route'=>'curso.curso', 'method' => 'POST']) !!}
                            <input value="{{$curso2->id}}" id="cursoid" name="cursoid" readonly style="display:none">
                            <input value="{{$curso2->professor_id}}" id="professorid" name="professorid" readonly style="display:none">
                            <p class="mt30"><button class="btn btn-primary" type="submit">Ver mais infomações</button> <span class="course-price">R$ {{$curso2->valor}}</span></p>
                            {!!Form:: close()!!}                        
                              </div>
                    </div>
                </div>
                @endforeach
                @foreach ($curso3 as $curso3)
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-course mb20">
                        <img src="uploads/cursos/{{$curso3->imagem}}" alt="">
                        <div class="course-details padding30">
                            <h3 class="font18">{{$curso3->titulo}}</h3>
                            <p>{{$curso3->minidescricao}}</p>
                            {!! Form:: open(['route'=>'curso.curso', 'method' => 'POST']) !!}
                            <input value="{{$curso3->id}}" id="cursoid" name="cursoid" readonly style="display:none">
                            <input value="{{$curso3->professor_id}}" id="professorid" name="professorid" readonly style="display:none">
                            <p class="mt30"><button class="btn btn-primary" type="submit">Ver mais infomações</button> <span class="course-price">R$ {{$curso3->valor}}</span></p>
                            {!!Form:: close()!!}                         
                             </div>
                    </div>
                </div>
                @endforeach
                @foreach ($curso4 as $curso4)
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-course mb20">
                        <img src="uploads/cursos/{{$curso4->imagem}}" alt="">
                        <div class="course-details padding30">
                            <h3 class="font18">{{$curso4->titulo}}</h3>
                            <p>{{$curso4->minidescricao}}</p>
                            {!! Form:: open(['route'=>'curso.curso', 'method' => 'POST']) !!}
                            <input value="{{$curso4->id}}" id="cursoid" name="cursoid" readonly style="display:none">
                            <input value="{{$curso4->professor_id}}" id="professorid" name="professorid" readonly style="display:none">
                            <p class="mt30"><button class="btn btn-primary" type="submit">Ver mais infomações</button> <span class="course-price">R$ {{$curso4->valor}}</span></p>
                            {!!Form:: close()!!}                       
                               </div>
                    </div>
                </div>
                @endforeach

                @foreach ($curso5 as $curso5)
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-course mb20">
                        <img src="uploads/cursos/{{$curso5->imagem}}" alt="">
                        <div class="course-details padding30">
                            <h3 class="font18">{{$curso5->titulo}}</h3>
                            <p>{{$curso5->minidescricao}}</p>
                            {!! Form:: open(['route'=>'curso.curso', 'method' => 'POST']) !!}
                            <input value="{{$curso5->id}}" id="cursoid" name="cursoid" readonly style="display:none">
                            <input value="{{$curso5->professor_id}}" id="professorid" name="professorid" readonly style="display:none">
                            <p class="mt30"><button class="btn btn-primary" type="submit">Ver mais infomações</button> <span class="course-price">R$ {{$curso5->valor}}</span></p>
                            {!!Form:: close()!!}                         
                             </div>
                    </div>
                </div>
                @endforeach

                @foreach ($curso6 as $curso6)
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-course mb20">
                        <img src="uploads/cursos/{{$curso6->imagem}}" alt="">
                        <div class="course-details padding30">
                            <h3 class="font18">{{$curso6->titulo}}</h3>
                            <p>{{$curso6->minidescricao}}</p>
                            {!! Form:: open(['route'=>'curso.curso', 'method' => 'POST']) !!}
                            <input value="{{$curso6->id}}" id="cursoid" name="cursoid" readonly style="display:none">
                            <input value="{{$curso6->professor_id}}" id="professorid" name="professorid" readonly style="display:none">
                            <p class="mt30"><button class="btn btn-primary" type="submit">Ver mais infomações</button> <span class="course-price">R$ {{$curso6->valor}}</span></p>
                            {!!Form:: close()!!}                         
                             </div>
                    </div>
                </div>
                @endforeach

                @foreach ($curso7 as $curso7)
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-course mb20">
                        <img src="uploads/cursos/{{$curso7->imagem}}" alt="">
                        <div class="course-details padding30">
                            <h3 class="font18">{{$curso7->titulo}}</h3>
                            <p>{{$curso7->minidescricao}}</p>
                            {!! Form:: open(['route'=>'curso.curso', 'method' => 'POST']) !!}
                            <input value="{{$curso7->id}}" id="cursoid" name="cursoid" readonly style="display:none">
                            <input value="{{$curso7->professor_id}}" id="professorid" name="professorid" readonly style="display:none">
                            <p class="mt30"><button class="btn btn-primary" type="submit">Ver mais infomações</button> <span class="course-price">R$ {{$curso7->valor}}</span></p>
                            {!!Form:: close()!!}                          
                            </div>
                    </div>
                </div>
                @endforeach

                @foreach ($curso8 as $curso8)
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-course mb20">
                        <img src="uploads/cursos/{{$curso8->imagem}}" alt="">
                        <div class="course-details padding30">
                            <h3 class="font18">{{$curso8->titulo}}</h3>
                            <p>{{$curso8->minidescricao}}</p>
                            {!! Form:: open(['route'=>'curso.curso', 'method' => 'POST']) !!}
                            <input value="{{$curso8->id}}" id="cursoid" name="cursoid" readonly style="display:none">
                            <input value="{{$curso8->professor_id}}" id="professorid" name="professorid" readonly style="display:none">
                            <p class="mt30"><button class="btn btn-primary" type="submit">Ver mais infomações</button> <span class="course-price">R$ {{$curso8->valor}}</span></p>
                            {!!Form:: close()!!}                          </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!--COURSE AREA END-->


    <!--TESTMONIAL AREA AREA-->
    <section class="testmonial-area bg-theme section-padding" id="testmonial">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2>Depoimentos</h2>
                        <p class="mt30"><button class="btn btn-primary" data-toggle="modal" data-target="#modalcomentario" type="submit" style="font-size:15px">Fale um pouco da sua experiência</button> </p>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                      @foreach ($depoimento1 as $depoimento1)
                    <div class="testmonial-slider">
                    <div class="single-testmonial">
                            <div class="author-content mb20">
                            <p style="color:black;font-size:20px"><b>{{$depoimento1->titulo}}</b></p>
                                <p>“{{$depoimento1->comentario}}”</p>
                            </div>
                            <div class="author-name-image relative">
                                <h5>{{$depoimento1->nome}}</h5>
                                <h4>nota: {{$depoimento1->nota}}</h4>

                            </div>
                        </div>
                        @endforeach

                        @foreach ($depoimento2 as $depoimento2)

                        <div class="single-testmonial">
                            <div class="author-content mb20">
                            <p style="color:black;font-size:20px"><b>{{$depoimento2->titulo}}</b></p>
                                <p>“{{$depoimento2->comentario}}”</p>
                            </div>
                            <div class="author-name-image relative">
                                <h5>{{$depoimento2->nome}}</h5>
                                <h4>nota: {{$depoimento2->nota}}</h4>

                            </div>
                            </div>
                        @endforeach

                        @foreach ($depoimento3 as $depoimento3)
                        <div class="single-testmonial">
                            <div class="author-content mb20">
                            <p style="color:black;font-size:20px"><b>{{$depoimento3->titulo}}</b></p>
                                <p>“{{$depoimento3->comentario}}”</p>
                            </div>
                            <div class="author-name-image relative">
                                <h4>{{$depoimento3->nome}}</h4>
                                <h4>nota: {{$depoimento3->nota}}</h4>

                            </div>
                            </div>
                        @endforeach

                        @foreach ($depoimento4 as $depoimento4)
                        <div class="single-testmonial">
                            <div class="author-content mb20">
                            <p style="color:black;font-size:20px"><b>{{$depoimento4->titulo}}</b></p>
                                <p>“{{$depoimento4->comentario}}”</p>
                            </div>
                            <div class="author-name-image relative">
                                <h4>{{$depoimento4->nome}}</h4>
                                <h4>nota: {{$depoimento4->nota}}</h4>

                            </div>
                            </div>
                        @endforeach

                        @foreach ($depoimento5 as $depoimento5)
                        <div class="single-testmonial">
                            <div class="author-content mb20">
                            <p style="color:black;font-size:20px"><b>{{$depoimento5->titulo}}</b></p>
                                <p>“{{$depoimento5->comentario}}”</p>
                            </div>
                            <div class="author-name-image relative">
                                <h4>{{$depoimento5->nome}}</h4>
                                <h4>nota: {{$depoimento5->nota}}</h4>

                            </div>
                            </div>
                        @endforeach

                        @foreach ($depoimento6 as $depoimento6)
                        <div class="single-testmonial">
                            <div class="author-content mb20">
                            <p style="color:black;font-size:20px"><b>{{$depoimento6->titulo}}</b></p>
                                <p>“{{$depoimento6->comentario}}”</p>
                            </div>
                            <div class="author-name-image relative">
                                <h4>{{$depoimento6->nome}}</h4>
                                <h4>nota: {{$depoimento6->nota}}</h4>

                            </div>
                            </div>
                        @endforeach

                        @foreach ($depoimento7 as $depoimento7)
                        <div class="single-testmonial">
                            <div class="author-content mb20">
                            <p style="color:black;font-size:20px"><b>{{$depoimento7->titulo}}</b></p>
                                <p>“{{$depoimento7->comentario}}”</p>
                            </div>
                            <div class="author-name-image relative">
                                <h4>{{$depoimento7->nome}}</h4>
                                <h4>nota: {{$depoimento7->nota}}</h4>

                            </div>
                            </div>
                        @endforeach

                        @foreach ($depoimento8 as $depoimento8)
                        <div class="single-testmonial">
                            <div class="author-content mb20">
                            <p style="color:black;font-size:20px"><b>{{$depoimento8->titulo}}</b></p>
                                <p>“{{$depoimento8->comentario}}”</p>
                            </div>
                            <div class="author-name-image relative">
                                <h4>{{$depoimento8->nome}}</h4>
                                <h4>nota:{{$depoimento8->nota}}</h4>

                            </div>
                            </div>
                        @endforeach 
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--TESTMONIAL AREA AREA END-->


    <!--BLOG AREA-->
    <section class="blog-feed-area padding-top" id="blog">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2>Clube Notícias</h2>
                    </div>
                </div>
            </div>
            <div class="row">
            @foreach ($blog1 as $blog1)
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="single-blog-item sm-mb30 xs-mb30 wow fadeInUp" data-wow-delay="0.2s">
                        <div class="blog-thumb">
                            <img src="uploads/blogs/{{$blog1->imagem}}" alt="">
                        </div>
                        <div class="blog-details padding30">
                            <h3 class="blog-title font20 mb30"><a href="blog.html">{{$blog1->titulo}}</a></h3>
                            <p class="blog-meta font14 mt20"><a href="#">Data da publicação: {{$blog1->data}}   </a></p>
                            <br>
                            <p><a href="{{$blog1->link}}" target="_blank"><button class="btn btn-primary"><b>ver publicação</b></button></a></p>
                        </div>
                    </div>
                </div>
                @endforeach

                @foreach ($blog2 as $blog2)
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="single-blog-item sm-mb30 xs-mb30 wow fadeInUp" data-wow-delay="0.3s">
                        <div class="blog-thumb">
                          <img src="uploads/blogs/{{$blog2->imagem}}" alt="">
                        </div>
                        <div class="blog-details padding30">
                            <h3 class="blog-title font20 mb30"><a href="blog.html">{{$blog2->titulo}}</a></h3>
                            <p class="blog-meta font14 mt20"><a href="#">Data da publicação: {{$blog2->data}}   </a></p>
                            <br>
                            <p><a href="{{$blog2->link}}"><button class="btn btn-primary"><b>ver publicação</b></button></a></p>
                        </div>
                    </div>
                </div>
                @endforeach

                @foreach ($blog3 as $blog3)
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="single-blog-item sm-mb30 xs-mb30 wow fadeInUp" data-wow-delay="0.4s">
                        <div class="blog-thumb">
                          <img src="uploads/blogs/{{$blog3->imagem}}" alt="">
                        </div>
                        <div class="blog-details padding30">
                            <h3 class="blog-title font20 mb30"><a href="blog.html">{{$blog3->titulo}}</a></h3>
                            <p class="blog-meta font14 mt20"><a href="#">Data da publicação: {{$blog3->data}}   </a></p>
                            <br>
                            <p><a href="{{$blog3->link}}"><button class="btn btn-primary"><b>ver publicação</b></button></a></p>
                        </div>
                    </div>
                </div>
                @endforeach

                @foreach ($blog4 as $blog4)
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="single-blog-item wow fadeInUp visible-sm" data-wow-delay="0.4s">
                        <div class="blog-thumb">
                           <img src="uploads/blogs/{{$blog4->imagem}}" alt="">
                        </div>
                        <div class="blog-details padding30">
                        <div class="blog-details padding30">
                            <h3 class="blog-title font20 mb30"><a href="blog.html">{{$blog4->titulo}}</a></h3>
                            <p class="blog-meta font14 mt20"><a href="#">Data da publicação: {{$blog4->data}}   </a></p>
                            <br>
                            <p><a href="{{$blog4->link}}"><button class="btn btn-primary"><b>ver publicação</b></button></a></p>
                        </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </section>
    <!--BLOG AREA END-->



    <!--FAQS AREA-->
    <section class="faqs-area padding-100-50" id="faqs">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2>FAQ</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.1s">
                        <h3 class="box-title">1. Fiz minha matrícula mas não irei participar?</h3>
                        <p class="text-justify">Você pode cancelar até 7 dias após a compar com reembolso total. Consulte a <a href=""><b>política de cancelamento</b>.</a> </p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.2s">
                        <h3 class="box-title">2. Tem certificado?</h3>
                        <p class="text-justify">Sim! Todos os nosso cursos possuem certificado de conclusão de curso. 
                        O mesmo é encaminhado para o e-mail cadastrado do aluno logo após finalizar o curso.</p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.3s">
                        <h3 class="box-title">3. Quem são os professores?</h3>
                        <p class="text-justify">Nosso corpo docente é formado por professores renomados e com uma vasta experiência na
                         área de tecnologia. Atuam na área acadêmica nas mais renomadas universidades e estão
                         sempre se atualizando em meio ao mercado.</p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.1s">
                        <h3 class="box-title">4. Disponibilizam material didático?</h3>
                        <p class="text-justify">Todo o material didático fica disponível por 3 meses para o aluno após o encerramento do curso.</p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.2s">
                        <h3 class="box-title">5. Qual a estrutura do curso?</h3>
                        <p class="text-justify">Cada curso tem uma estrutura específica, os curso presenciais são diferentes dos EAD.
                        Na página do curso tem suas devidades especificações.</p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.3s">
                        <h3 class="box-title">6. Tem alguma outra taxa além da matrícula ?</h3>
                        <p class="text-justify">Não! Você irá pagar uma única taxa, podendo parcelar o valor de acordo com os meios de pagamentos (boleto ou cartão de crédito). 
                        Existem casos particulares como o curso de AWS que possuem taxas de serviço.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--FAQS AREA END-->

     <!--CONTACT US AREA-->
     <section class="contact-area sky-gray-bg" id="contact">
        <div class="container-fluid no-padding">
            <div class="row no-margin">
                <div class="no-padding col-md-6 col-lg-6 col-sm-12 col-xs-12">
                    <div class="map-area relative">
                        <div id="map" style="width: 100%; height: 600px;"> </div>
                    </div>
                </div>
                <div class="no-padding col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <div class="contact-form-content padding50 xs-padding20">
                        <div class="contact-title wow fadeIn">
                            <h3 class="font28 mb50 xs-mb30 xs-font22 xs-mt20"> Tem alguma dúvida? Escreve pra gente!  &nbsp;:)</h3>
                        </div>
                        <div class="contact-form wow fadeIn">
                        {!! Form:: open(['route'=>'lead.save', 'method' => 'POST']) !!}                               
                         <div class="row">
                                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                        <div class="form-group" id="name-field">
                                            <div class="form-input">
                                                <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome Completo" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                        <div class="form-group" id="email-field">
                                            <div class="form-input">
                                                <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefone" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <input type="text" class="form-control" id="assunto" name="assunto" placeholder="Assunto" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="message-field">
                                            <div class="form-input">
                                                <textarea class="form-control" rows="6" id="mensagem" name="mensagem" placeholder="Escreva sua mensagem..." required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group mb0">
                                            <button type="submit"  >Enviar mensagem</button>
                                        </div>
                                    </div>
                                </div>
                                {!!Form:: close()!!}                      
                              </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--CONTACT US AREA END-->

    <!--CLIENT AREA-->
    <div class="client-area  padding-bottom mt100 sm-mt10 xs-mt0">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="client-slider">
                        <div class="single-client">
                            <img src="img/client/temet.png" alt="">
                        </div>
                        <div class="single-client">
                            <img src="img/client/cron.png" alt="">
                        </div>
                        <div class="single-client">
                            <img src="img/client/clube360.png" alt="">
                        </div>
                        <div class="single-client">
                            <img src="img/client/empregos.png" alt="">
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--CLIENT AREA END-->

    <!--ADDMISSION AREA-->
    <section class="admition-area padding-50-50 bg-theme">
        <div class="container">
            <div class="row flex-v-center">
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <div class="admition-content xs-center xs-mb30">
                        <h3 class="xs-font20">Quer fazer parte do nosso time?</h3>
                        <p>O clube Tech quer você como professor no nosso time, preencha o formulário de inscrição.</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                    <div class="enroll-button right xs-center">
                        <button class="time"  data-toggle="modal" data-target="#modalprofessor">cadastre-se</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--ADDMISSION AREA END-->

    @extends('partials.footer')


    <!--====== SCRIPTS JS ======-->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>

    <!--====== PLUGINS JS ======-->
    <script src="js/vendor/jquery.easing.1.3.js"></script>
    <script src="js/vendor/jquery-migrate-1.2.1.min.js"></script>
    <script src="js/vendor/jquery.appear.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/stellar.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery-modal-video.min.js"></script>
    <script src="js/stellarnav.min.js"></script>
    <script src="js/placeholdem.min.js"></script>
    <script src="js/contact-form.js"></script>
    <script src="js/jquery.ajaxchimp.js"></script>
    <script src="js/jquery.sticky.js"></script>

    <!--===== ACTIVE JS=====-->
    <script src="js/main.js"></script>

    <!--===== MAPS JS=====-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTS_KEDfHXYBslFTI_qPJIybDP3eceE-A&sensor=false"></script>
    <script src="js/maps.active.js"></script>


    <script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }


  if (n <= 0) {
    document.getElementById("submit").style.display = "none";
    
  } else {
    document.getElementById("submit").style.display = "inline";
    document.getElementById("submit").innerHTML = "Próximo";

  }


  if (n == 1) {
    document.getElementById("nextBtn").style.display = "none";


  } else {
    document.getElementById("nextBtn").style.display = "inline";
    document.getElementById("nextBtn").innerHTML = "Próximo";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
</script>



<script type="text/javascript">
	$("#cep").focusout(function(){
		//Início do Comando AJAX
		$.ajax({
			//O campo URL diz o caminho de onde virá os dados
			//É importante concatenar o valor digitado no CEP
			url: 'https://viacep.com.br/ws/'+$(this).val()+'/json/unicode/',
			//Aqui você deve preencher o tipo de dados que será lido,
			//no caso, estamos lendo JSON.
			dataType: 'json',
			//SUCESS é referente a função que será executada caso
			//ele consiga ler a fonte de dados com sucesso.
			//O parâmetro dentro da função se refere ao nome da variável
			//que você vai dar para ler esse objeto.
			success: function(resposta){
				//Agora basta definir os valores que você deseja preencher
				//automaticamente nos campos acima.
				$("#logradouro").val(resposta.logradouro);
				$("#complemento").val(resposta.complemento);
				$("#bairro").val(resposta.bairro);
				$("#cidade").val(resposta.localidade);
				$("#uf").val(resposta.uf);
				//Vamos incluir para que o Número seja focado automaticamente
				//melhorando a experiência do usuário
				$("#numero").focus();
			}
		});
	});
</script>

 <!-- WhatsHelp.io widget -->
 <script type="text/javascript">
    (function () {
    var options = {
    whatsapp: "5571993771555", // WhatsApp number
    call_to_action: "Fale com a nossa equipe !", // Call to action
    button_color: "#666666", // Color of button
    position: "right", // Position may be 'right' or 'left'
    order: "whatsapp", // Order of buttons
    };
    var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
    var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
    s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
    var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
    </script>
    <!-- /WhatsHelp.io widget -->




<script>
      $('#cliente').chosen({
    placeholder_text_single : 'Procure seu banco'
});
      </script>
</body>

</html>
