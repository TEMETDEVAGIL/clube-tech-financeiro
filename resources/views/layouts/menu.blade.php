<li class="header" style="color:white"><b>MENU</b> </li>



<li class="treeview" style="bacground-color: #000">
<a href="#">
  <i class=" fa fa-circle-o "></i>
  <span>Site</span>
  <span class="pull-right-container">
    <i class="fa fa-angle-left pull-right"></i>
  </span>
</a>
<ul class="treeview-menu">

<li class="{{ Request::is('banners*') ? 'active' : '' }}">
    <a href="{{ route('banners.index') }}"><i class="fa fa-edit"></i><span>Banners</span></a>
</li>


<li class="{{ Request::is('cursos*') ? 'active' : '' }}">
    <a href="{{ route('cursos.index') }}"><i class="fa fa-edit"></i><span>Cursos</span></a>
</li>

<li class="{{ Request::is('blogs*') ? 'active' : '' }}">
    <a href="{{ route('blogs.index') }}"><i class="fa fa-edit"></i><span>Blogs</span></a>
</li>



</ul>

<li class="treeview" style="bacground-color: #000">
<a href="#">
  <i class=" fa fa-circle-o "></i>
  <span>Financeiro</span>
  <span class="pull-right-container">
    <i class="fa fa-angle-left pull-right"></i>
  </span>
</a>
<ul class="treeview-menu">
<li class="{{ Request::is('contasapagars*') ? 'active' : '' }}">
    <a href="{{ route('contasapagars.index') }}"><i class="fa fa-edit"></i><span>Contas a pagar</span></a>
</li>

<li class="{{ Request::is('contasarecebers*') ? 'active' : '' }}">
    <a href="{{ route('contasarecebers.index') }}"><i class="fa fa-edit"></i><span>Contas a receber</span></a>
</li>

<li class="{{ Request::is('contasarecebers*') ? 'active' : '' }}">
    <a href="relatorioscontaspagas"><i class="fa fa-edit"></i><span>Relatório Contas Pagas</span></a>
</li>

<li class="{{ Request::is('contasarecebers*') ? 'active' : '' }}">
    <a href="relatorioscontasrecebidas"><i class="fa fa-edit"></i><span>Relatório Contas Recebidas</span></a>
</li>


</ul>

<li class="treeview" style="bacground-color: #000">
<a href="#">
  <i class=" fa fa-circle-o "></i>
  <span>Administração</span>
  <span class="pull-right-container">
    <i class="fa fa-angle-left pull-right"></i>
  </span>
</a>
<ul class="treeview-menu">
<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{{ route('users.index') }}"><i class="fa fa-edit"></i><span>Configurações</span></a>
</li>

<li class="{{ Request::is('empresas*') ? 'active' : '' }}">
    <a href="{{ route('empresas.index') }}"><i class="fa fa-edit"></i><span>Empresas</span></a>
</li>

<li class="{{ Request::is('clientes*') ? 'active' : '' }}">
    <a href="{{ route('clientes.index') }}"><i class="fa fa-edit"></i><span>Clientes</span></a>
</li>

<li class="{{ Request::is('professors*') ? 'active' : '' }}">
    <a href="{{ route('professors.index') }}"><i class="fa fa-edit"></i><span>Professores</span></a>
</li>

<li class="{{ Request::is('leads*') ? 'active' : '' }}">
    <a href="{{ route('leads.index') }}"><i class="fa fa-edit"></i><span>Leads</span></a>
</li>

<li class="{{ Request::is('depoimentos*') ? 'active' : '' }}">
    <a href="{{ route('depoimentos.index') }}"><i class="fa fa-edit"></i><span>Depoimentos</span></a>
</li>

