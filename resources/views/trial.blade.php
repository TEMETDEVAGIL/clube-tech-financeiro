<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <!--====== USEFULL META ======-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="APPTON HTML5 Template is a simple Smooth Personal App Landing Template" />
    <meta name="keywords" content="App, Landing, Business, Onepage, Html, Business" />

    <!--====== TITLE TAG ======-->
    <title>Clube Tech | Sua escola de tecnologia</title>

    <!--====== FAVICON ICON =======-->
    <link rel="shortcut icon" type="image/ico" href="img/favicon.png" />

    <!--====== STYLESHEETS ======-->
    <link href="css/plugins.css" rel="stylesheet">
    <link href="css/theme.css" rel="stylesheet">
    <link href="css/icons.css" rel="stylesheet">

    <!--====== MAIN STYLESHEETS ======-->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <script src="js/vendor/modernizr-2.8.3.min.js"></script>

    <!-- ====== Modal POP UP =================-->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>


<!--============ chosen ========== -->
<link href="http://harvesthq.github.io/chosen/chosen.css" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<script src="http://harvesthq.github.io/chosen/chosen.jquery.js"></script>

<!--============ SWAL ========== -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '602826070441240');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=602826070441240&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '593660518022210');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=593660518022210&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->



  
</head>

 <!--POP UP-->
 <?php
			$situacao_usuario = "pendente";
			if($situacao_usuario == "pendente"){ ?>
				<script>
					$(document).ready(function(){
						$('#myModal').modal('show');
					});
				</script>
            <?php } ?>

          
            
            

<body data-spy="scroll" data-target=".mainmenu-area" data-offset="90">

<!-- Modal pop up -->
<div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:3%">
		<div class="modal-dialog " role="document" style="background-color:#63c8d0">
          <div class="modal-content">
            <div class="modal-header" style="background-color:white">
            <a href="http://bit.ly/clubetech"><i class="fa fa-facebook-square fa-2x face" aria-hidden="true"></i></a>
            &nbsp;<a href="http://bit.ly/clubetech"><i class="fa fa-instagram fa-2x insta" aria-hidden="true"></i></a>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:red"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" style="background-color:#63c8d0">
			<div class="container-fluid">
    <div class="row">
        <div class="col-12 mt-3">
            <div class="">
                <div class="card-horizontal" >
                    <div class="img-square-wrapper">
                        <img class="img-popup" src="img/logo.png" alt="Card image cap" style="width:30%">
                    </div>
                    <br>
                    <div class="card-body pose-popup" >
                        <h4 class="card-title" style="font-size:20px;color:white">Uma nova forma de aprender Tecnologia !</h4><br>
                        <h4 class="card-title" style="font-size:20px;color:white">Click no ícone e Fale com a gente ! <br><a href="http://bit.ly/clubetech"><i class="fa fa-whatsapp fa-5x whats" aria-hidden="true" style="color:white" ></i></a></h4>
						
      </div>
     </div>
                </div>  
            </div>
        </div>
    </div>
</div>          
 </div>
   </div>
</div>


    <!--- PRELOADER -->
    <div class="preeloader">
        <div class="preloader-spinner"></div>
    </div>

    <!--SCROLL TO TOP-->
    <a href="#scroolup" class="scrolltotop"><i class="fa fa-long-arrow-up"></i></a>

    <!--START TOP AREA-->


  
    <header class="top-area" id="home">
        <div class="header-top-area" id="scroolup">
            <!--MAINMENU AREA-->
            <div class="mainmenu-area" id="mainmenu-area">
                <div class="mainmenu-area-bg"></div>
                <nav class="navbar">
                    <div class="container">
                        <div class="navbar-header">
                            <a href="#home" class="navbar-brand"><img src="img/logo.png" alt="logo" style="width:19%"></a>
                        </div>
                        <div id="main-nav" class="stellarnav">
                        <ul id="nav" class="nav navbar-nav pull-right">
                                <li class="active"><a  style="color:white">Email<br><p>cursos@clube-tech.com</p></a>
                                </li>
                                <li class="active"><a  style="color:white">Telefone<br><p>(71) 9 8199-8284  <i class="fa fa-whatsapp" aria-hidden="true"></i></p></a>
                                </li>
                                <li class="active"><a  style="color:white">Horário de funcionamento<br><p>segunda - sexta 8h - 18h</p></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <!--END MAINMENU AREA END-->
        </div>

        <!--WELCOME SLIDER AREA-->
        <div class="welcome-slider-area white font16">
            <div class="welcome-single-slide">
                <div class="slide-bg-overlay" style="background: rgba(0, 0, 0, 0) url('img/trial/bg_1.jpg') no-repeat scroll center center / cover;"></div>

                <div class="welcome-area">
                    <div class="container">
                        <div class="row flex-v-center">
                            <div class="col-md-8 col-lg-7 col-sm-12 col-xs-12">
                                <div class="welcome-text" style="margin-top:10%" >
                                <h1> AULAS ONLINE </h1>
                                    <p style="font-size:25px"> Aprimore seu conhecimento, exercícios práticos e trabalho de conclusão de curso</p>
                                    <br>
                                    <div class="home-button">
                                    <a heref="https://www.sympla.com.br/clubetech"><button class="button button4"><b style="font-size:20px">Nossos cursos</b></button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="welcome-single-slide">
            <div class="slide-bg-overlay" style="background: rgba(0, 0, 0, 0) url('img/trial/bg_2.jpg') no-repeat scroll center center / cover;"></div>

                <div class="welcome-area">
                    <div class="container">
                        <div class="row flex-v-center">
                            <div class="col-md-8 col-lg-7 col-sm-12 col-xs-12">
                                <div class="welcome-text" style="margin-top:10%">
                                <h1> Clube = agência = TECNOLOGIA = inovação = você  </h1>
                                    <p style="font-size:25px">Aumente sua rede de contato, aqui no ClubeTech temos uma rede com todos os alunos. Vem pro clube!</p>
                                    <div class="home-button">
                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
        <!--WELCOME SLIDER AREA END-->
    </header>
    <!--END TOP AREA-->

    <!--FEATURES TOP AREA-->
    <section class="features-top-area" id="features">
        <div class="container">
            <div class="row promo-content">
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box mb20 xs-mb0 wow fadeInUp padding30" data-wow-delay="0.1s">
                        <div class="box-icon features-box-icon" style="background-color:#63c8d0">
                            <i class="fa fa-graduation-cap" style="color:white" ></i>
                        </div>
                        <h3 class="box-title">Cursos</h3>
                        <p class="text-justify">Sua qualificação profissional, com qualidade e eficiência. Cursos, treinamentos, workshops e palestras para o mercado tech.</p>
                        <br>
                        <a href="https://www.sympla.com.br/clubetech"> <button class="button1 button4"><b style="font-size:15px"><span style="color:white">Nossos cursos</span></b></button></a>

                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb20 xs-mb0  wow fadeInUp padding30" data-wow-delay="0.2s">
                    <div class="box-icon features-box-icon" style="background-color:#63c8d0">
                            <i class="icofont icofont-business-man-alt-1" style="color:white"></i>
                        </div>
                        <h3 class="box-title">Consultoria</h3>
                        <p class="text-justify">Profissionais que tem o propósito de levantar as necessidades dos alunos, identificar soluções e recomendar ações de um determinado problema.</p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb20 xs-mb0 wow fadeInUp padding30" data-wow-delay="0.3s">
                    <div class="box-icon features-box-icon" style="background-color:#63c8d0">
                            <i class="fa fa-rocket" style="color:white"></i>
                        </div>
                        <h3 class="box-title">Coworking</h3>
                        <p class="text-justify"> Espaço colaborativo. Seu escritório virtual e presencial.</p>
                    </div>
                </div>
               
            </div>
        </div>
    </section>
    <!--FEATURES TOP AREA END-->
    <br>

      <!--FOOER AREA-->
  <footer class="footer-area sky-gray-bg relative" >
        <div class="footer-top-area padding-80-80 bg-dark">
            <div class="container">
                <div class="row col-md-12">
                    <div class="col-md-6">
                        <div class="single-footer footer-about sm-mb50 xs-mb50 sm-center xs-center">
                            <div class="footer-logo mb30">
                                <a href="#"><img src="img/logo.png" alt="" style="width:50%"></a>
                            </div>
                            <p class="text-justify"><b>Edtech para profissionais e apaixonados por tecnologia</b>. 
<br>
Nossa missão é a acompanhar o profissional de tecnologia de ponta a ponta, desde a  qualificação profissional ao encaminhamento para o mercado de trabalho, assim formando um ecossistema de profissionais da tecnologia preparados para o futuro.
</p>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="single-footer footer-subscribe white xs-center">
                            <h3 class="mb30 xs-font18">Não perca a oportunidade</h3>
                            <p>Matricule-se em um curso e se destaque no mercado de trabalho.</p>
                      </section>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom-area white">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="footer-copyright text-center wow fadeIn">
                            
Copyright &copy;<script>document.write(new Date().getFullYear());</script> | Somos apaixonado por tecnologia e VOCÊ ?  |  Desenvolvido </i>  por<a href="http://temet.com.br/" target="_blank"> Temet Desenvolvimento</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--FOOER AREA END-->


    <!--====== SCRIPTS JS ======-->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>

    <!--====== PLUGINS JS ======-->
    <script src="js/vendor/jquery.easing.1.3.js"></script>
    <script src="js/vendor/jquery-migrate-1.2.1.min.js"></script>
    <script src="js/vendor/jquery.appear.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/stellar.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery-modal-video.min.js"></script>
    <script src="js/stellarnav.min.js"></script>
    <script src="js/placeholdem.min.js"></script>
    <script src="js/contact-form.js"></script>
    <script src="js/jquery.ajaxchimp.js"></script>
    <script src="js/jquery.sticky.js"></script>

    <!--===== ACTIVE JS=====-->
    <script src="js/main.js"></script>

    <!--===== MAPS JS=====-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTS_KEDfHXYBslFTI_qPJIybDP3eceE-A&sensor=false"></script>
    <script src="js/maps.active.js"></script>


    <script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }


  if (n <= 0) {
    document.getElementById("submit").style.display = "none";
    
  } else {
    document.getElementById("submit").style.display = "inline";
    document.getElementById("submit").innerHTML = "Próximo";

  }


  if (n == 1) {
    document.getElementById("nextBtn").style.display = "none";


  } else {
    document.getElementById("nextBtn").style.display = "inline";
    document.getElementById("nextBtn").innerHTML = "Próximo";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
</script>


<script type="text/javascript">
	$("#cep").focusout(function(){
		//Início do Comando AJAX
		$.ajax({
			//O campo URL diz o caminho de onde virá os dados
			//É importante concatenar o valor digitado no CEP
			url: 'https://viacep.com.br/ws/'+$(this).val()+'/json/unicode/',
			//Aqui você deve preencher o tipo de dados que será lido,
			//no caso, estamos lendo JSON.
			dataType: 'json',
			//SUCESS é referente a função que será executada caso
			//ele consiga ler a fonte de dados com sucesso.
			//O parâmetro dentro da função se refere ao nome da variável
			//que você vai dar para ler esse objeto.
			success: function(resposta){
				//Agora basta definir os valores que você deseja preencher
				//automaticamente nos campos acima.
				$("#logradouro").val(resposta.logradouro);
				$("#complemento").val(resposta.complemento);
				$("#bairro").val(resposta.bairro);
				$("#cidade").val(resposta.localidade);
				$("#uf").val(resposta.uf);
				//Vamos incluir para que o Número seja focado automaticamente
				//melhorando a experiência do usuário
				$("#numero").focus();
			}
		});
	});
</script>

<script>
      $('#cliente').chosen({
    placeholder_text_single : 'Procure seu banco'
});
      </script>
</body>

</html>
