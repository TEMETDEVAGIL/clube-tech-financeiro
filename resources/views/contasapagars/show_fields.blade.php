<!-- Empresa Id Field -->
<div class="form-group col-md-6">
    {!! Form::label('empresa_id', 'Empresa :') !!}
    <p>{{ $contasapagar->empresa_id }}</p>
</div>

<!-- Datavencimento Field -->
<div class="form-group col-md-6">
    {!! Form::label('datavencimento', 'Data de vencimento:') !!}
    <p>{{ $contasapagar->datavencimento }}</p>
</div>

<!-- Datapagamento Field -->
<div class="form-group col-md-6">
    {!! Form::label('datapagamento', 'Data de pagamento:') !!}
    <p>{{ $contasapagar->datapagamento }}</p>
</div>

<!-- Valor Field -->
<div class="form-group col-md-6">
    {!! Form::label('valor', 'Valor:') !!}
    <p>{{ $contasapagar->valor }}</p>
</div>

<!-- Status Field -->
<div class="form-group col-md-6">
    {!! Form::label('status', 'Status:') !!}

    @if ($contasapagar->status == '1')
          <p >Pago</p>
            @else 
            <p>Pendente</p>

            @endif
</div>

