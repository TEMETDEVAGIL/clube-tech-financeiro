<!-- Empresa Id Field -->

<div class="form-group col-sm-6">
    {!! Form::label('empresa_id', 'Selecione a Empresa:') !!}
    <select name="empresa_id" class="form-control">
    <option >Empresa</option>

    @foreach ($empresas as $emp)
    <option name="empresas" value="{{ $emp->id }}" >{{ $emp->razaosocial }}</option>
    @endforeach
  </select>
</div>


<!-- Datavencimento Field -->
<div class="form-group col-sm-6">
    {!! Form::label('datavencimento', 'Data de vencimento:') !!}
    {!! Form::date('datavencimento', null, ['class' => 'form-control']) !!}
</div>

<!-- Datapagamento Field -->
<div class="form-group col-sm-6">
    {!! Form::label('datapagamento', 'Data de pagamento:') !!}
    {!! Form::date('datapagamento', null, ['class' => 'form-control']) !!}
</div>

<!-- Valor Field -->
<div class="form-group col-sm-6">
    {!! Form::label('valor', 'Valor:') !!}
    {!! Form::number('valor', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Pago:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('status', 0) !!}
        {!! Form::checkbox('status', '1', null) !!}
    </label>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-success']) !!}
    <a href="{{ route('contasapagars.index') }}" class="btn btn-danger">Cancelar</a>
</div>
