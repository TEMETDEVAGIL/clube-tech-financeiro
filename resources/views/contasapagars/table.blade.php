<div class="table-responsive">
    <table class="table" id="contasapagars-table">
        <thead>
            <tr>
                <th>Empresa</th>
                <th>Data de vencimento</th>
                <th>Data de pagamento</th>
                <th>Valor</th>
                <th>Status</th>
                <th colspan="3">Ações</th>
            </tr>
        </thead>
        <tbody>
       
       
            <tr>
            @foreach($contasapagars as $contasapagar)

            @if ($contasapagar->empresa_id == '1')
                <td class="text-center">Tim</td>
            @else 
            <td class="text-center">Não cadastrado</td>

            @endif

            <td>{{ $contasapagar->datavencimento }}</td>
            <td>{{ $contasapagar->datapagamento }}</td>
            <td>{{ $contasapagar->valor }}</td>

            @if ($contasapagar->status == '1')
                <td class="text-center" style=" color: #2E8B57;"><b>Pago</b></td>
            @else ($contasapagar->status == '0')
                <td class="text-center" style=" color: #FFD700;"><b>Pendente</b></td>
            @endif

                <td>
                    {!! Form::open(['route' => ['contasapagars.destroy', $contasapagar->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('contasapagars.show', [$contasapagar->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('contasapagars.edit', [$contasapagar->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
