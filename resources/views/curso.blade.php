<!doctype html>

<html class="no-js" lang="zxx">
<!--<![endif]-->

<head>
    <!--====== USEFULL META ======-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Alinhamos as experiências profissionais com métodos educacionais para obter uma metodologia de aula que tenha foco na qualificação dos alunos de forma integral." />
    <meta name="keywords" content="Curso, Tecnologia, online" />


    <!--====== TITLE TAG ======-->
    <title>Clube Tech | Sua escola de tecnologia</title>

    <!--====== FAVICON ICON =======-->
    <link rel="shortcut icon" type="image/ico" href="img/favicon.png" />

    <!--====== STYLESHEETS ======-->
    <link href="css/plugins.css" rel="stylesheet">
    <link href="css/theme.css" rel="stylesheet">
    <link href="css/icons.css" rel="stylesheet">

    <!--====== MAIN STYLESHEETS ======-->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    <script src="js/jquery.countdown.js"></script>
    <script src="js/jquery.countdown.min.js"></script>

    
</head>

<body data-spy="scroll" data-target=".mainmenu-area" data-offset="90">
    <!--- PRELOADER -->
    <div class="preeloader">
        <div class="preloader-spinner"></div>
    </div>

    <!--SCROLL TO TOP-->
    <a href="#scroolup" class="scrolltotop"><i class="fa fa-long-arrow-up"></i></a>

    <!--START TOP AREA-->
    <header class="top-area" id="home">
        <div class="header-top-area" id="scroolup">
            <!--MAINMENU AREA-->
            <div class="mainmenu-area" id="mainmenu-area">
                <div class="mainmenu-area-bg"></div>
                <nav class="navbar navbar-inverse" style="background-color:#292929" id="nav">
                    <ul class="nav navbar-nav">
                        <li><a href="/"><img src="img/logo.png" style="width:10%"></a></li>
                    </ul>
                </nav> 
            </div>
            <!--END MAINMENU AREA END-->
        </div>

        <!--WELCOME SLIDER AREA-->
        @foreach ($idcurso as $curso)
        <div class="page-header" style="margin-top:-2px ;background: rgba(0, 0, 0, 0) url('uploads/banners/{{$curso->banner}}') no-repeat scroll center center / cover;">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                    <div class="page-caption" >
                        <h1 class="page-title">{{$curso->titulo}}</h1>
                        <br>
                       <center> <h4  style="color:white">{{$curso->fraseefeito}}</h4></center>
                    
                        <br><br>
                        <center><p id="demo" style="  font-size: 40px;color:white"></p></center>

                    </div>
                </div>
            </div>
        </div>
    </div> 
    
        <!--WELCOME SLIDER AREA END-->
    </header>
    <!--END TOP AREA-->

    <!--FEATURES TOP AREA-->
    <section class="features-top-area" id="features">
        <div class="container ">
            <div class="row promo-content col-md-12">
                <div class="col-md-3 ">
                    <div class="text-icon-box mb20 xs-mb0 wow fadeInUp padding30" data-wow-delay="0.1s">
                        <div class="box-icon features-box-icon">
                        <i class="icofont icofont-time"></i>                        
                       </div>
                       <h4 >Tempo Estimado</h4>
                        <p style="font-size:15px"><b>{{$curso->cargahoraria}}</b></p>
                        <p  style="font-size:12px">30 horas presencias + 10 horas online</p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="text-icon-box relative mb20 xs-mb0  wow fadeInUp padding30" data-wow-delay="0.2s">
                        <div class="box-icon features-box-icon">
                        <i class="icofont icofont-calendar"></i>                      
                      </div>
                      <h4 >Datas</h4>
                        <p style="font-size:15px">{{$curso->data}}</p>
                    </div>
                </div>

                <div class="col-md-3 ">
                    <div class="text-icon-box relative mb20 xs-mb0  wow fadeInUp padding30" data-wow-delay="0.2s">
                        <div class="box-icon features-box-icon">
                        <i class="icofont icofont-users"></i>                      
                      </div>
                      <h4 >Para quem é este curso</h4>
                        <p style="font-size:15px" >{{$curso->publico}}</p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="text-icon-box relative mb20 xs-mb0 wow fadeInUp padding30" data-wow-delay="0.3s">
                        <div class="box-icon features-box-icon">
                        <i class="fa fa-list"></i>
                        </div>
                        <h4 >Pré-requisitos</h4>
                        <p style="font-size:15px" >{{$curso->requisitos}}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--FEATURES TOP AREA END-->

    <!--CLIENT AREA-->
    <div class="jumbotron jumbotron-fluid" style="background-image: url('img/vemproclube.png');height:34rem;background-repeat:no-repeat">
  <div class="container">
   
  </div>
</div>
    <!--CLIENT AREA END-->

    <!--ABOUT AREA-->
    <section class="section-padding about-area" id="about">
        <div class="container" style="margin-top:-6%">
            <div class="row flex-v-center">
                <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12">
                    <div class="about-content xs-mb50 wow fadeIn">
                    <h3 class="xs-font20 mb30" style="color:#63c8d0"><b>{{$curso->titulo}}</b></h3>
                        <p class="text-justify">{{$curso->minidescricao}}</p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-md-offset-1 col-lg-offset-1 col-sm-12 col-xs-12">
                    <div class="video-promo-details wow fadeIn">
                        <img src="uploads/cursos/{{$curso->imagem}}" alt="">
                    </div>
                </div>
            </div>
        </div>

    
        <div class="container" style="margin-top:6%">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="single-blog-item sm-mb30 xs-mb30 wow fadeInUp" data-wow-delay="0.2s">
                        <div class="blog-details padding30">
                        <center><h3 class="blog-title font20 mb30">O que irá Aprender !</h3></center>
                            <p class="blog-meta font14 mt20 text-justify">{{$curso->descricao}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="single-blog-item sm-mb30 xs-mb30 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="accordions"> 
                        <br>
                    <center><h3 class="blog-title font20 mb30">Conteúdo do curso</h3></center>

                    @if ($curso->modulo1 == '')
                    <div class="accordion-item" >
              <input type="radio" name="accordion" checked="checked" id="accordion-1" />
              <center><label for= "accordion-1" style="color:white;font-size:18px;display:none">Módulo 1</label></label></center>
              <div class="accordion-content" style="display:none">{{$curso->modulo1}}</div>
          </div> 
          @else 
               <div class="accordion-item"  >
              <input type="radio" name="accordion" checked="checked" id="accordion-1" />
              <center><label for= "accordion-1" style="color:white;font-size:18px;display:show">Módulo 1</label></label></center>
              <div class="accordion-content" style="display:show">{{$curso->modulo1}}</div>
          </div> 

            @endif

            @if ($curso->modulo2 == '')
          <div class="accordion-item">
              <input type="radio" name="accordion" id="accordion-2" />
              <center><label for= "accordion-2" style="color:white;font-size:18px;display:none">Módulo 2</label></label></center>
              <div class="accordion-content"style="display:none" >{{$curso->modulo2}}</div>
          </div> 
          @else 

          <div class="accordion-item">
              <input type="radio" name="accordion" id="accordion-2" />
              <center><label for= "accordion-2" style="color:white;font-size:18px;display:show">Módulo 2</label></label></center>
              <div class="accordion-content"style="display:show" >{{$curso->modulo2}}</div>
          </div> 
          @endif

          @if ($curso->modulo3 == '')
          <div class="accordion-item">
              <input type="radio" name="accordion" id="accordion-3" />
              <center><label for= "accordion-3" style="color:white;font-size:18px;display:none">Módulo 3</label></label></center>
              <div class="accordion-content" style="display:none">{{$curso->modulo3}}</div>
          </div>
          @else 
          <div class="accordion-item">
              <input type="radio" name="accordion" id="accordion-3" />
              <center><label for= "accordion-3" style="color:white;font-size:18px;display:show">Módulo 3</label></label></center>
              <div class="accordion-content" style="display:show">{{$curso->modulo3}}</div>
          </div>
        
          @endif

          @if ($curso->modulo4 == '')
          <div class="accordion-item">
              <input type="radio" name="accordion" id="accordion-4" />
              <center><label for= "accordion-4" style="color:white;font-size:18px;display:none">Módulo 4</label></label></center>
              <div class="accordion-content" style="display:none">{{$curso->modulo4}}</div>
          </div>
          @else 
          <div class="accordion-item">
              <input type="radio" name="accordion" id="accordion-4" />
              <center><label for= "accordion-4" style="color:white;font-size:18px;display:show">Módulo 4</label></label></center>
              <div class="accordion-content" style="display:show">{{$curso->modulo4}}</div>
          </div>
        
          @endif

          @if ($curso->modulo5 == '')
          <div class="accordion-item">
              <input type="radio" name="accordion" id="accordion-5" />
              <center><label for= "accordion-5" style="color:white;font-size:18px;display:none">Módulo 5</label></label></center>
              <div class="accordion-content" style="display:none">{{$curso->modulo5}}</div>
          </div>
          @else 
          <div class="accordion-item">
              <input type="radio" name="accordion" id="accordion-5" />
              <center><label for= "accordion-5" style="color:white;font-size:18px;display:show">Módulo 5</label></label></center>
              <div class="accordion-content" style="display:show">{{$curso->modulo5}}</div>
          </div>
        
          @endif

          @if ($curso->modulo6 == '')
          <div class="accordion-item">
              <input type="radio" name="accordion" id="accordion-6" />
              <center><label for= "accordion-6" style="color:white;font-size:18px;display:none">Módulo 6</label></label></center>
              <div class="accordion-content" style="display:none">{{$curso->modulo6}}</div>
          </div>
          @else 
          <div class="accordion-item">
              <input type="radio" name="accordion" id="accordion-6" />
              <center><label for= "accordion-6" style="color:white;font-size:18px;display:show">Módulo 6</label></label></center>
              <div class="accordion-content" style="display:show">{{$curso->modulo6}}</div>
          </div>
        
          @endif

          @if ($curso->modulo7 == '')
          <div class="accordion-item">
              <input type="radio" name="accordion" id="accordion-7" />
              <center><label for= "accordion-7" style="color:white;font-size:18px;display:none">Módulo 7</label></label></center>
              <div class="accordion-content" style="display:none">{{$curso->modulo7}}</div>
          </div>
          @else 
          <div class="accordion-item">
              <input type="radio" name="accordion" id="accordion-7" />
              <center><label for= "accordion-7" style="color:white;font-size:18px;display:show">Módulo 7</label></label></center>
              <div class="accordion-content" style="display:show">{{$curso->modulo7}}</div>
          </div>
        
          @endif

          @if ($curso->modulo8 == '')
          <div class="accordion-item">
              <input type="radio" name="accordion" id="accordion-8" />
              <center><label for= "accordion-8" style="color:white;font-size:18px;display:none">Módulo 8</label></label></center>
              <div class="accordion-content" style="display:none">{{$curso->modulo8}}</div>
          </div>
          @else 
          <div class="accordion-item">
              <input type="radio" name="accordion" id="accordion-8" />
              <center><label for= "accordion-8" style="color:white;font-size:18px;display:show">Módulo 8</label></label></center>
              <div class="accordion-content" style="display:show">{{$curso->modulo8}}</div>
          </div>
        
          @endif

          @if ($curso->modulo9 == '')
          <div class="accordion-item">
              <input type="radio" name="accordion" id="accordion-9" />
              <center><label for= "accordion-9" style="color:white;font-size:18px;display:none">Módulo 9</label></label></center>
              <div class="accordion-content" style="display:none">{{$curso->modulo9}}</div>
          </div>
          @else 
          <div class="accordion-item">
              <input type="radio" name="accordion" id="accordion-9" />
              <center><label for= "accordion-9" style="color:white;font-size:18px;display:show">Módulo 9</label></label></center>
              <div class="accordion-content" style="display:show">{{$curso->modulo9}}</div>
          </div>
        
          @endif

           @if ($curso->modulo10 == '')
          <div class="accordion-item">
              <input type="radio" name="accordion" id="accordion-10" />
              <center><label for= "accordion-10" style="color:white;font-size:18px;display:none">Módulo 10</label></label></center>
              <div class="accordion-content" style="display:none">{{$curso->modulo10}}</div>
          </div>
          @else 
          <div class="accordion-item">
              <input type="radio" name="accordion" id="accordion-5" />
              <center><label for= "accordion-10" style="color:white;font-size:18px;display:show">Módulo 10</label></label></center>
              <div class="accordion-content" style="display:show">{{$curso->modulo10}}</div>
          </div>
        
          @endif
         
      </div>	
                    </div>
                </div>
                @endforeach
                @foreach ($professor as $professor)

                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="single-blog-item sm-mb30 xs-mb30 wow fadeInUp" data-wow-delay="0.4s">
                        <div class="blog-details padding30">
                       <center> <h3 class="blog-title font20 mb30">Instrutor</h3></center>
                      
                        <div class="circle" style="margin-left: 33%">
                        <img src="uploads/professores/{{$professor->imagem}}" alt="">
                       </div>
                       
                            <p class="blog-meta font14 mt20 text-justify"><b style="color:#63c8d0">{{$professor->nome}}</b></p>
                            
                            <p class="blog-meta font14 mt20 text-justify">{{$professor->minicv}}</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   <center> 

   @endforeach

   @foreach ($idcurso as $curso)

                    <div class="area-title text-center wow fadeIn">
                        <h2><b>Inscreva-se</b></h2>
                    </div>


   <div id="sympla-widget-855659" height="auto" ></div> 
   <script src="https://www.sympla.com.br/js/sympla.widget-pt.js/{{$curso->link}}"></script>

   @endforeach

 
    <!--ABOUT AREA END-->

    <!--TESTMONIAL AREA AREA-->
    <section class="testmonial-area bg-theme section-padding" id="testmonial" style="background-color:white;margin-top:-5%">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2>Todos os nossos cursos possuem</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.1s">
                        <h3 class="box-title"><i class="fa fa-check" aria-hidden="true" style="color:green"></i> Two Very Different Australias?</h3>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.2s">
                        <h3 class="box-title"><i class="fa fa-check" aria-hidden="true" style="color:green"></i>  What Does Darwin Mean to You?</h3>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.3s">
                        <h3 class="box-title"><i class="fa fa-check" aria-hidden="true" style="color:green"></i>  Where Totem Poles Are a Living Art ?</h3>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.1s">
                        <h3 class="box-title"><i class="fa fa-check" aria-hidden="true" style="color:green"></i>  Two Very Different Australias?</h3>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.2s">
                        <h3 class="box-title"><i class="fa fa-check" aria-hidden="true" style="color:green"></i>  What Does Darwin Mean to You?</h3>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box relative mb50 wow fadeInUp" data-wow-delay="0.3s">
                        <h3 class="box-title"><i class="fa fa-check" aria-hidden="true" style="color:green"></i> Where Totem Poles Are a Living Art ?</h3>
                    </div>
                </div>
                
                </div>
            </div>
        </div>
    </section>
    <!--TESTMONIAL AREA AREA END-->


    @extends('partials.footer')



    <!--====== SCRIPTS JS ======-->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>

    <!--====== PLUGINS JS ======-->
    <script src="js/vendor/jquery.easing.1.3.js"></script>
    <script src="js/vendor/jquery-migrate-1.2.1.min.js"></script>
    <script src="js/vendor/jquery.appear.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/stellar.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery-modal-video.min.js"></script>
    <script src="js/stellarnav.min.js"></script>
    <script src="js/placeholdem.min.js"></script>
    <script src="js/contact-form.js"></script>
    <script src="js/jquery.ajaxchimp.js"></script>
    <script src="js/jquery.sticky.js"></script>

    <!--===== ACTIVE JS=====-->
    <script src="js/main.js"></script>

    <!--===== MAPS JS=====-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTS_KEDfHXYBslFTI_qPJIybDP3eceE-A&sensor=false"></script>
    <script src="js/maps.active.js"></script>


    <script>
// Set the date we're counting down to
@foreach ($idcurso as $curso)
var countDownDate = new Date("{{$curso->contadormes}} {{$curso->contadordia}}, {{$curso->contadorano}} {{$curso->contadorhorario}}").getTime();
@endforeach


// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("demo").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";


    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = " vendas encerradas";
  }
}, 1000);
</script>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>
</body>

</html>
