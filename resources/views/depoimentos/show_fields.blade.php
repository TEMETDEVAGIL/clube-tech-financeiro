<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $depoimentos->email }}</p>
</div>

<!-- Nome Field -->
<div class="form-group">
    {!! Form::label('nome', 'Nome:') !!}
    <p>{{ $depoimentos->nome }}</p>
</div>

<!-- Curso Id Field -->
<div class="form-group">
    {!! Form::label('curso_id', 'Curso Id:') !!}
    <p>{{ $depoimentos->curso_id }}</p>
</div>

<!-- Comentario Field -->
<div class="form-group">
    {!! Form::label('comentario', 'Comentario:') !!}
    <p>{{ $depoimentos->comentario }}</p>
</div>

<!-- Nota Field -->
<div class="form-group">
    {!! Form::label('nota', 'Nota:') !!}
    <p>{{ $depoimentos->nota }}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $depoimentos->status }}</p>
</div>

