@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Depoimentos
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($depoimentos, ['route' => ['depoimentos.update', $depoimentos->id], 'method' => 'patch']) !!}

                        @include('depoimentos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection