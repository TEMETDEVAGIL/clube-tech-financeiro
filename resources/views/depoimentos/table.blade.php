<div class="table-responsive">
    <table class="table" id="depoimentos-table">
        <thead>
            <tr>
                <th>Email</th>
        <th>Nome</th>
        <th>Curso Id</th>
        <th>Comentario</th>
        <th>Nota</th>
        <th>Data</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($depoimentos as $depoimentos)
            <tr>
                <td>{{ $depoimentos->email }}</td>
            <td>{{ $depoimentos->nome }}</td>
            <td>{{ $depoimentos->curso_id }}</td>
            <td>{{ $depoimentos->comentario }}</td>
            <td>{{ $depoimentos->nota }}</td>
            <td>{{ $depoimentos->created_at }}</td>
            <td>{{ $depoimentos->status }}</td>
                <td>
                    {!! Form::open(['route' => ['depoimentos.destroy', $depoimentos->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('depoimentos.show', [$depoimentos->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('depoimentos.edit', [$depoimentos->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
