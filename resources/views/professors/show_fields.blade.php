<!-- Nome Field -->
<div class="form-group">
    {!! Form::label('nome', 'Nome:') !!}
    <p>{{ $professors->nome }}</p>
</div>

<!-- Cpf Field -->
<div class="form-group">
    {!! Form::label('cpf', 'Cpf:') !!}
    <p>{{ $professors->cpf }}</p>
</div>

<!-- Telefone Field -->
<div class="form-group">
    {!! Form::label('telefone', 'Telefone:') !!}
    <p>{{ $professors->telefone }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $professors->email }}</p>
</div>

<!-- Minicv Field -->
<div class="form-group">
    {!! Form::label('minicv', 'Minicv:') !!}
    <p>{{ $professors->minicv }}</p>
</div>

<!-- Imagem Field -->
<div class="form-group">
    {!! Form::label('imagem', 'Imagem:') !!}
    <p>{{ $professors->imagem }}</p>
</div>

<!-- Cep Field -->
<div class="form-group">
    {!! Form::label('cep', 'Cep:') !!}
    <p>{{ $professors->cep }}</p>
</div>

<!-- Logradouro Field -->
<div class="form-group">
    {!! Form::label('logradouro', 'Logradouro:') !!}
    <p>{{ $professors->logradouro }}</p>
</div>

<!-- Bairro Field -->
<div class="form-group">
    {!! Form::label('bairro', 'Bairro:') !!}
    <p>{{ $professors->bairro }}</p>
</div>

<!-- Cidade Field -->
<div class="form-group">
    {!! Form::label('cidade', 'Cidade:') !!}
    <p>{{ $professors->cidade }}</p>
</div>

<!-- Uf Field -->
<div class="form-group">
    {!! Form::label('uf', 'Uf:') !!}
    <p>{{ $professors->uf }}</p>
</div>

<!-- Complemento Field -->
<div class="form-group">
    {!! Form::label('complemento', 'Complemento:') !!}
    <p>{{ $professors->complemento }}</p>
</div>

<!-- Banco Field -->
<div class="form-group">
    {!! Form::label('banco', 'Banco:') !!}
    <p>{{ $professors->banco }}</p>
</div>

<!-- Agencia Field -->
<div class="form-group">
    {!! Form::label('agencia', 'Agencia:') !!}
    <p>{{ $professors->agencia }}</p>
</div>

<!-- Conta Field -->
<div class="form-group">
    {!! Form::label('conta', 'Conta:') !!}
    <p>{{ $professors->conta }}</p>
</div>

