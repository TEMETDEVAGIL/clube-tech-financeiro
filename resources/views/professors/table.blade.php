<div class="table-responsive">
    <table class="table" id="professors-table">
        <thead>
            <tr>
                <th>Nome</th>
        <th>Cpf</th>
        <th>Telefone</th>
        <th>Email</th>
        <th>Minicv</th>
        <th>Upload de imagem</th>
                <th colspan="3">Ações</th>
            </tr>
        </thead>
        <tbody>
        @foreach($professors as $professors)
            <tr>
            <td>{{ $professors->nome }}</td>
            <td>{{ $professors->cpf }}</td>
            <td>{{ $professors->telefone }}</td>
            <td>{{ $professors->email }}</td>
            <td>{{ $professors->minicv }}</td>
            <td>  
                <div class='btn-group'>
                    <a href="uploadProfessor" class='btn btn-success btn-xs'>Adicione uma imagem <i class="glyphicon glyphicon-picture"></i></a>
                </div>
                </td>
           
                <td>
                    {!! Form::open(['route' => ['professors.destroy', $professors->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('professors.show', [$professors->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('professors.edit', [$professors->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
