@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Professors
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($professors, ['route' => ['professors.update', $professors->id], 'method' => 'patch']) !!}

                        @include('professors.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection