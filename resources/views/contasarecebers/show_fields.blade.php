<!-- Cliente Id Field -->
<div class="form-group col-md-6">
    {!! Form::label('cliente_id', 'Cliente :') !!}
    <p>{{ $contasareceber->cliente_id }}</p>
</div>

<!-- Datarecebimento Field -->
<div class="form-group col-md-6">
    {!! Form::label('datarecebimento', 'Data de recebimento:') !!}
    <p>{{ $contasareceber->datarecebimento }}</p>
</div>

<!-- Valor Field -->
<div class="form-group col-md-6">
    {!! Form::label('valor', 'Valor:') !!}
    <p>{{ $contasareceber->valor }}</p>
</div>

<!-- Status Field -->
<div class="form-group col-md-6">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $contasareceber->status }}</p>
</div>

