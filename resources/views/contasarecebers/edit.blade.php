@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
        <b>Contas a receber</b>

        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($contasareceber, ['route' => ['contasarecebers.update', $contasareceber->id], 'method' => 'patch']) !!}

                        @include('contasarecebers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection