<!-- Cliente Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cliente_id', 'Selecione o Cliente:') !!}
    <select name="cliente_id" class="form-control">
    <option >Cliente</option>

    @foreach ($clientes as $cli)
    <option name="clientes" value="{{ $cli->id }}" >{{ $cli->nome }}</option>
    <option name="clientes" value="{{ $cli->id }}" >{{ $cli->razaosocial }}</option>
    @endforeach
  </select>
</div>

<!-- Datarecebimento Field -->
<div class="form-group col-sm-6">
    {!! Form::label('datarecebimento', 'Data de recebimento:') !!}
    {!! Form::date('datarecebimento', null, ['class' => 'form-control']) !!}
</div>

<!-- Valor Field -->
<div class="form-group col-sm-6">
    {!! Form::label('valor', 'Valor:') !!}
    {!! Form::number('valor', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('status', 0) !!}
        {!! Form::checkbox('status', '1', null) !!}
    </label>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-success']) !!}
    <a href="{{ route('contasarecebers.index') }}" class="btn btn-danger">Cancelar</a>
</div>
