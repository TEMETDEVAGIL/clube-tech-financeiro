<div class="table-responsive">
    <table class="table" id="contasarecebers-table">
        <thead>
            <tr>
                <th>Cliente</th>
                <th>Data de recebimento</th>
                <th>Valor</th>
                <th>Status</th>
                <th colspan="3">Ações</th>
            </tr>
        </thead>
        <tbody>
        @foreach($contasarecebers as $contasareceber)
            <tr>
            @if ($contasareceber->cliente_id == '1')
                <td class="text-center">Teste</td>
            @else 
            <td class="text-center">Não cadastrado</td>

            @endif
            <td>{{ $contasareceber->datarecebimento }}</td>
            <td>{{ $contasareceber->valor }}</td>

            @if ($contasareceber->status == '1')
                <td class="text-center" style=" color: #2E8B57;"><b>Recebido</b></td>
            @else ($contasareceber->status == '0')
                <td class="text-center" style=" color: #FFD700;"><b>Pendente</b></td>
            @endif
            
                <td>
                    {!! Form::open(['route' => ['contasarecebers.destroy', $contasareceber->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('contasarecebers.show', [$contasareceber->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('contasarecebers.edit', [$contasareceber->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
