@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">            
        <b>Relatórios Contas Pagas 2020</b>
</h1>
        
    </section>
    <div class="content">
        <div class="clearfix"></div>
        <div class="box box-primary">  
        </div>

        <div class="container">
    <div class="row">

    <div class="row" style="margin-top:2%">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>Janeiro</h3>

             <a href="relatoriojaneiropaga" ><button class="btn btn-primary"> Gerar Relatório</button></a>

            </div>
           
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3> Fevereiro</h3>

            <a href="relatoriofevereiropaga" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
           
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Março</h3>

            <a href="relatoriomarcopaga" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Abril</h3>

            <a href="relatorioabrilpaga" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
            
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Maio</h3>

            <a href="relatoriomaiopaga" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
            
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Junho</h3>

            <a href="relatoriojunhopaga" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
            
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Julho</h3>

            <a href="relatoriojulhopaga" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
            
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Agosto</h3>

            <a href="relatorioagostopaga" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
            
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Setembro</h3>

            <a href="relatoriosetembropaga" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
            
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Outubro</h3>

            <a href="relatoriooutubropaga" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
            
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Novembro</h3>

            <a href="relatorionovembropaga" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
            
          </div>
        </div>


        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h3>Dezembro</h3>

            <a href="relatoriodezembropaga" ><button class="btn btn-primary"> Gerar Relatório</button></a>
            </div>
            
          </div>
        </div>
       
      </div>
    </div>
</div>
        
       
    </div>
@endsection

