@extends('layouts.app')

@section('content')

@if(Session::has('fail'))
    <div class="alert alert-warning">
       {{Session::get('fail')}}
    </div>
@endif
<div class="container">
    <div class="row">

    <div class="row" style="margin-top:2%">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$clientescount}}</h3>

              <p>Clientes</p>
            </div>
            <div class="icon">
              <i class="ion ion-person"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{$empresascount}}</h3>

              <p>Empresas</p>
            </div>
            <div class="icon">
              <i class="ion ion-home"></i>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{$contasapagarcount}}</h3>

              <p>Contas a pagar</p>
            </div>
            <div class="icon">
              <i class="ion ion-calendar"></i>
            </div>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{$contasarecebercount}}</h3>

              <p>Contas a receber</p>
            </div>
            <div class="icon">
              <i class="ion ion-calendar"></i>
            </div>
          </div>
        </div>
       
      </div>
    </div>
</div>
@endsection
