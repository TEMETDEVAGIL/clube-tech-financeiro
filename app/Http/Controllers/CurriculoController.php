<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Support\Facades\Input as input; 

class CurriculoController extends Controller
{
    public function upload(Request $request)
    {
        if(Request::hasFile('file')){

            $file = Request::file('file');
            $file->move('uploads/curriculos', $file->getClientOriginalName());

            return redirect('/')->withFail('Cadastro realizado com sucesso!  Aguarde o nosso contato');;

        }
        else{
            return redirect('curriculo')->withFail('Erro ao enviar');;


        }


        }
}
