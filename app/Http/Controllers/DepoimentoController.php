<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Aluno;
use App\Depoimento;
use Redirect;
use DB;  

class DepoimentoController extends Controller
{

    public function savedepoimento(Request $req){
        
        $depoimento = new Depoimento;
        $depoimento ->email = $req->email;
        $depoimento ->curso_id = $req->curso;
        $depoimento ->nome = $req->nome;
        $depoimento ->nota = $req->nota;
        $depoimento ->comentario = $req->comentario;
      
       
      
        $depoimento->save();
        return Redirect::to("/")->withFail('Comentario cadastrado com sucesso !');
    }

   
    }

