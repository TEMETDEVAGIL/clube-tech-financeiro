<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use DB; 


class PdfContasrecebidasController extends Controller
{
    public function relatoriojaneiro()
    {
        $relatoriojaneiro = DB::table('contasareceber')
    ->select('contasareceber.*')
    ->whereBetween('datarecebimento', array('2020-01-01', '2020-01-31'))->where('status',1)->get();
     
    //total
    $totaljaneiro =DB::table("contasareceber")->whereBetween('datarecebimento', array('2020-01-01', '2020-01-31'))->where('status',1)->get()->sum("valor");

     
        return \PDF::loadView('Relatoriocontasrecebidas.janeiro', compact('relatoriojaneiro','totaljaneiro'))
                    // Se quiser que fique no formato a4 retrato:
                         ->setPaper('a4', 'landscape')
                    ->download('relatorio.pdf');

                    return redirect('index');

    }

    public function relatoriofevereiro()
    {
        $relatoriofevereiro = DB::table('contasareceber')
    ->select('contasareceber.*')
    ->whereBetween('datarecebimento', array('2020-02-01', '2020-02-31'))->where('status',1)->get();

     //total
      $totalfevereiro = DB::table("contasareceber")->whereBetween('datarecebimento', array('2020-02-01', '2020-02-31'))->where('status',1)->get()->sum("valor");
     
        return \PDF::loadView('Relatoriocontasrecebidas.fevereiro', compact('relatoriofevereiro','totalfevereiro'))
                    // Se quiser que fique no formato a4 retrato:
                         ->setPaper('a4', 'landscape')
                    ->download('relatorio.pdf');
                    return redirect('index');


    }

    public function relatoriomarco()
    {
        $relatoriomarco = DB::table('contasareceber')
    ->select('contasareceber.*')
    ->whereBetween('datarecebimento', array('2020-03-01', '2020-03-31'))->where('status',1)->get();

     //total
      $totalmarco = DB::table("contasareceber")->whereBetween('datarecebimento', array('2020-03-01', '2020-03-31'))->where('status',1)->get()->sum("valor");
     
        return \PDF::loadView('Relatoriocontasrecebidas.marco', compact('relatoriomarco','totalmarco'))
                    // Se quiser que fique no formato a4 retrato:
                         ->setPaper('a4', 'landscape')
                    ->download('relatorio.pdf');
                    return redirect('index');


    }

    public function relatorioabril()
    {
        $relatorioabril = DB::table('contasareceber')
    ->select('contasareceber.*')
    ->whereBetween('datarecebimento', array('2020-04-01', '2020-04-31'))->where('status',1)->get();

     //total
      $totalabril = DB::table("contasareceber")->whereBetween('datarecebimento', array('2020-04-01', '2020-04-31'))->where('status',1)->get()->sum("valor");
     
        return \PDF::loadView('Relatorios.abril', compact('relatorioabril','totalabril'))
                    // Se quiser que fique no formato a4 retrato:
                         ->setPaper('a4', 'landscape')
                    ->download('Relatoriocontasrecebidas.pdf');
                    return redirect('index');


    }

    public function relatoriomaio()
    {
        $relatoriomaio = DB::table('contasareceber')
    ->select('contasareceber.*')
    ->whereBetween('datarecebimento', array('2020-05-01', '2020-05-31'))->where('status',1)->get();

     //total
      $totalmaio = DB::table("contasareceber")->whereBetween('datarecebimento', array('2020-05-01', '2020-05-31'))->where('status',1)->get()->sum("valor");
     
        return \PDF::loadView('Relatoriocontasrecebidas.maio', compact('relatoriomaio','totalmaio'))
                    // Se quiser que fique no formato a4 retrato:
                         ->setPaper('a4', 'landscape')
                    ->download('relatorio.pdf');
                    return redirect('index');


    }

    public function relatoriojunho()
    {
        $relatoriojunho = DB::table('contasareceber')
    ->select('contasareceber.*')
    ->whereBetween('datarecebimento', array('2020-06-01', '2020-06-31'))->where('status',1)->get();

     //total
      $totaljunho = DB::table("contasareceber")->whereBetween('datarecebimento', array('2020-06-01', '2020-06-31'))->where('status',1)->get()->sum("valor");
     
        return \PDF::loadView('Relatoriocontasrecebidas.junho', compact('relatoriojunho','totaljunho'))
                    // Se quiser que fique no formato a4 retrato:
                         ->setPaper('a4', 'landscape')
                    ->download('relatorio.pdf');
                    return redirect('index');


    }

    public function relatoriojulho()
    {
        $relatoriojulho = DB::table('contasareceber')
    ->select('contasareceber.*')
    ->whereBetween('datarecebimento', array('2020-07-01', '2020-07-31'))->where('status',1)->get();

     //total
      $totaljulho = DB::table("contasareceber")->whereBetween('datarecebimento', array('2020-07-01', '2020-07-31'))->where('status',1)->get()->sum("valor");
     
        return \PDF::loadView('Relatoriocontasrecebidas.julho', compact('relatoriojulho','totaljulho'))
                    // Se quiser que fique no formato a4 retrato:
                         ->setPaper('a4', 'landscape')
                    ->download('relatorio.pdf');
                    return redirect('index');


    }


    public function relatorioagosto()
    {
        $relatorioagosto = DB::table('contasareceber')
    ->select('contasareceber.*')
    ->whereBetween('datarecebimento', array('2020-08-01', '2020-08-31'))->where('status',1)->get();

     //total
      $totalagosto = DB::table("contasareceber")->whereBetween('datarecebimento', array('2020-08-01', '2020-08-31'))->where('status',1)->get()->sum("valor");
     
        return \PDF::loadView('Relatoriocontasrecebidas.agosto', compact('relatorioagosto','totalagosto'))
                    // Se quiser que fique no formato a4 retrato:
                         ->setPaper('a4', 'landscape')
                    ->download('relatorio.pdf');
                    return redirect('index');


    }

    public function relatoriosetembro()
    {
        $relatoriosetembro = DB::table('contasareceber')
    ->select('contasareceber.*')
    ->whereBetween('datarecebimento', array('2020-09-01', '2020-09-31'))->where('status',1)->get();

     //total
      $totalsetembro = DB::table("contasareceber")->whereBetween('datarecebimento', array('2020-09-01', '2020-09-31'))->where('status',1)->get()->sum("valor");
     
        return \PDF::loadView('Relatoriocontasrecebidas.setembro', compact('relatoriosetembro','totalsetembro'))
                    // Se quiser que fique no formato a4 retrato:
                         ->setPaper('a4', 'landscape')
                    ->download('relatorio.pdf');
                    return redirect('index');


    }


    public function relatoriooutubro()
    {
        $relatoriooutubro = DB::table('contasareceber')
    ->select('contasareceber.*')
    ->whereBetween('datarecebimento', array('2020-10-01', '2020-10-31'))->where('status',1)->get();

     //total
      $totaloutubro = DB::table("contasareceber")->whereBetween('datarecebimento', array('2020-10-01', '2020-10-31'))->where('status',1)->get()->sum("valor");
     
        return \PDF::loadView('Relatoriocontasrecebidas.outubro', compact('relatoriooutubro','totaloutubro'))
                    // Se quiser que fique no formato a4 retrato:
                         ->setPaper('a4', 'landscape')
                    ->download('relatorio.pdf');
                    return redirect('index');


    }

    public function relatorionovembro()
    {
        $relatorionovembro = DB::table('contasareceber')
    ->select('contasareceber.*')
    ->whereBetween('datarecebimento', array('2020-11-01', '2020-11-31'))->where('status',1)->get();

     //total
      $totalnovembro = DB::table("contasareceber")->whereBetween('datarecebimento', array('2020-11-01', '2020-11-31'))->where('status',1)->get()->sum("valor");
     
        return \PDF::loadView('Relatoriocontasrecebidas.novembro', compact('relatorionovembro','totalnovembro'))
                    // Se quiser que fique no formato a4 retrato:
                         ->setPaper('a4', 'landscape')
                    ->download('relatorio.pdf');
                    return redirect('index');


    }

    public function relatoriodezembro()
    {
        $relatoriodezembro = DB::table('contasareceber')
    ->select('contasareceber.*')
    ->whereBetween('datarecebimento', array('2020-12-01', '2020-12-31'))->where('status',1)->get();

     //total
      $totaldezembro = DB::table("contasareceber")->whereBetween('datarecebimento', array('2020-12-01', '2020-12-31'))->where('status',1)->get()->sum("valor");
     
        return \PDF::loadView('Relatoriocontasrecebidas.dezembro', compact('relatoriodezembro','totaldezembro'))
                    // Se quiser que fique no formato a4 retrato:
                         ->setPaper('a4', 'landscape')
                    ->download('relatorio.pdf');
                    return redirect('index');


    }
}
