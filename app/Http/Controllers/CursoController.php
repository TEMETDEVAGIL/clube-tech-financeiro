<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use DB; 
use App\Curso;


class CursoController extends Controller
{
    public function curso(Request $request)
    {

    $idcurso = $request->input('cursoid');
    $idprofessor = $request->input('professorid');

    $idcurso = DB::table('cursos')
    ->select('cursos.*')
    ->where('cursos.id', $idcurso) ->get();
    


       $professor = DB::table('cursos')
       ->join('professors', 'professors.id', '=', 'cursos.professor_id')
       ->select('professors.*')
       ->where('cursos.id', $idprofessor)->get();



   
    

    return view('curso')
    ->with(compact('idcurso','professor'));                  

    
    }
    
}
