<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatecontasapagarRequest;
use App\Http\Requests\UpdatecontasapagarRequest;
use App\Repositories\contasapagarRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Empresa;

class contasapagarController extends AppBaseController
{
    /** @var  contasapagarRepository */
    private $contasapagarRepository;

    public function __construct(contasapagarRepository $contasapagarRepo)
    {
        $this->contasapagarRepository = $contasapagarRepo;
    }

    /**
     * Display a listing of the contasapagar.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $contasapagars = $this->contasapagarRepository->all();

    
        return view('contasapagars.index')
            ->with('contasapagars', $contasapagars);
    }

    /**
     * Show the form for creating a new contasapagar.
     *
     * @return Response
     */
    public function create()
    {
        $empresas = Empresa::all();
        return view('contasapagars.create', compact('empresas'))->with('empresas ', $empresas );
    }

    /**
     * Store a newly created contasapagar in storage.
     *
     * @param CreatecontasapagarRequest $request
     *
     * @return Response
     */
    public function store(CreatecontasapagarRequest $request)
    {
        $input = $request->all();

        $contasapagar = $this->contasapagarRepository->create($input);

        Flash::success('Conta  salva com sucesso.');

        return redirect(route('contasapagars.index'));
    }

    /**
     * Display the specified contasapagar.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $contasapagar = $this->contasapagarRepository->find($id);

        if (empty($contasapagar)) {
            Flash::error('Conta  não encontrada');

            return redirect(route('contasapagars.index'));
        }

        return view('contasapagars.show')->with('contasapagar', $contasapagar);
    }

    /**
     * Show the form for editing the specified contasapagar.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $contasapagar = $this->contasapagarRepository->find($id);
        $empresas = Empresa::all();


        if (empty($contasapagar)) {
            Flash::error('Conta  não encontrada');

            return redirect(route('contasapagars.index'));
        }

        return view('contasapagars.edit')->with('contasapagar', $contasapagar)->with('empresas', $empresas);
    }

    /**
     * Update the specified contasapagar in storage.
     *
     * @param int $id
     * @param UpdatecontasapagarRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecontasapagarRequest $request)
    {
        $contasapagar = $this->contasapagarRepository->find($id);

        if (empty($contasapagar)) {
            Flash::error('Conta  não encontrada');

            return redirect(route('contasapagars.index'));
        }

        $contasapagar = $this->contasapagarRepository->update($request->all(), $id);

        Flash::success('Conta atualizada com sucesso.');

        return redirect(route('contasapagars.index'));
    }

    /**
     * Remove the specified contasapagar from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $contasapagar = $this->contasapagarRepository->find($id);

        if (empty($contasapagar)) {
            Flash::error('Conta  não encontrada');

            return redirect(route('contasapagars.index'));
        }

        $this->contasapagarRepository->delete($id);

        Flash::success('Contas deletada com sucesso.');

        return redirect(route('contasapagars.index'));
    }

    
}
