<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateprofessorsRequest;
use App\Http\Requests\UpdateprofessorsRequest;
use App\Repositories\professorsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class professorsController extends AppBaseController
{
    /** @var  professorsRepository */
    private $professorsRepository;

    public function __construct(professorsRepository $professorsRepo)
    {
        $this->professorsRepository = $professorsRepo;
    }

    /**
     * Display a listing of the professors.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $professors = $this->professorsRepository->all();

        return view('professors.index')
            ->with('professors', $professors);
    }

    /**
     * Show the form for creating a new professors.
     *
     * @return Response
     */
    public function create()
    {
        return view('professors.create');
    }

    /**
     * Store a newly created professors in storage.
     *
     * @param CreateprofessorsRequest $request
     *
     * @return Response
     */
    public function store(CreateprofessorsRequest $request)
    {
        $input = $request->all();

        $professors = $this->professorsRepository->create($input);

        Flash::success('Professors saved successfully.');

        return redirect(route('professors.index'));
    }

    /**
     * Display the specified professors.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $professors = $this->professorsRepository->find($id);

        if (empty($professors)) {
            Flash::error('Professors not found');

            return redirect(route('professors.index'));
        }

        return view('professors.show')->with('professors', $professors);
    }

    /**
     * Show the form for editing the specified professors.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $professors = $this->professorsRepository->find($id);

        if (empty($professors)) {
            Flash::error('Professors not found');

            return redirect(route('professors.index'));
        }

        return view('professors.edit')->with('professors', $professors);
    }

    /**
     * Update the specified professors in storage.
     *
     * @param int $id
     * @param UpdateprofessorsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateprofessorsRequest $request)
    {
        $professors = $this->professorsRepository->find($id);

        if (empty($professors)) {
            Flash::error('Professors not found');

            return redirect(route('professors.index'));
        }

        $professors = $this->professorsRepository->update($request->all(), $id);

        Flash::success('Professors updated successfully.');

        return redirect(route('professors.index'));
    }

    /**
     * Remove the specified professors from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $professors = $this->professorsRepository->find($id);

        if (empty($professors)) {
            Flash::error('Professors not found');

            return redirect(route('professors.index'));
        }

        $this->professorsRepository->delete($id);

        Flash::success('Professors deleted successfully.');

        return redirect(route('professors.index'));
    }
}
