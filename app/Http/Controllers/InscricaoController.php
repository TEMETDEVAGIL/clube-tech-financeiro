<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Professor;
use Redirect;
use DB;  

class InscricaoController extends Controller
{
    public function save(Request $req){

        $idcpf= $req->input('cpf');
        

        if (DB::table('professors')->where('cpf', $idcpf)->count('cpf') == 0) {

        $professor = new Professor;
        $professor->nome = $req->nome;
        $professor->cpf = $req->cpf;
        $professor->email = $req->email;
        $professor->telefone = $req->telefone;
        $professor->minicv = $req->minicv;
        $professor->cep = $req->cep;
        $professor->logradouro = $req->logradouro;
        $professor->bairro = $req->bairro;
        $professor->cidade = $req->cidade;
        $professor->uf = $req->uf;
        $professor->complemento = $req->complemento;
        $professor->banco = $req->banco;
        $professor->agencia = $req->agencia;
        $professor->conta = $req->conta;
        $professor->imagem = $req->imagem;
      
        $professor->save();
        return Redirect::to("curriculo");
    }

        else{
            return Redirect::to("/")->withFail('CPF já cadastrado!  Procure um respónsavel do Clube');
        }


    }
}
