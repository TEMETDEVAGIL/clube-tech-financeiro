<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Support\Facades\Input as input; 

class UploadController extends Controller
{
    public function uploadBanner(Request $request)
    {
        if(Request::hasFile('file')){

            $file = Request::file('file');
            $file->move('uploads/banners', $file->getClientOriginalName());

            return redirect('home')->withFail('Imagem salva com sucesso');

        }
    }

        public function uploadCurso(Request $request)
        {
            if(Request::hasFile('file')){
    
                $file = Request::file('file');
                $file->move('uploads/cursos', $file->getClientOriginalName());
    
                return redirect('home')->withFail('Imagem salva com sucesso');

    
            }
       
    }

    public function uploadBlog(Request $request)
    {
        if(Request::hasFile('file')){

            $file = Request::file('file');
            $file->move('uploads/blogs', $file->getClientOriginalName());

            return redirect('home')->withFail('Imagem salva com sucesso');


        }
   
}

public function uploadProfessor(Request $request)
    {
        if(Request::hasFile('file')){

            $file = Request::file('file');
            $file->move('uploads/professores', $file->getClientOriginalName());

            return redirect('home')->withFail('Imagem salva com sucesso');


        }
   
}


}
