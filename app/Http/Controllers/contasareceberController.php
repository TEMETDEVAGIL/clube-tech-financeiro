<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatecontasareceberRequest;
use App\Http\Requests\UpdatecontasareceberRequest;
use App\Repositories\contasareceberRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Cliente;


class contasareceberController extends AppBaseController
{
    /** @var  contasareceberRepository */
    private $contasareceberRepository;

    public function __construct(contasareceberRepository $contasareceberRepo)
    {
        $this->contasareceberRepository = $contasareceberRepo;
    }

    /**
     * Display a listing of the contasareceber.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $contasarecebers = $this->contasareceberRepository->all();

        return view('contasarecebers.index')
            ->with('contasarecebers', $contasarecebers);
    }

    /**
     * Show the form for creating a new contasareceber.
     *
     * @return Response
     */
    public function create()
    {
        $clientes = Cliente::all();
        return view('contasarecebers.create', compact('clientes'))->with('clientes ', $clientes );
    }

    /**
     * Store a newly created contasareceber in storage.
     *
     * @param CreatecontasareceberRequest $request
     *
     * @return Response
     */
    public function store(CreatecontasareceberRequest $request)
    {
        $input = $request->all();

        $contasareceber = $this->contasareceberRepository->create($input);

        Flash::success('Contasareceber saved successfully.');

        return redirect(route('contasarecebers.index'));
    }

    /**
     * Display the specified contasareceber.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $contasareceber = $this->contasareceberRepository->find($id);

        if (empty($contasareceber)) {
            Flash::error('Contasareceber not found');

            return redirect(route('contasarecebers.index'));
        }

        return view('contasarecebers.show')->with('contasareceber', $contasareceber);
    }

    /**
     * Show the form for editing the specified contasareceber.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $contasareceber = $this->contasareceberRepository->find($id);
        $clientes = Cliente::all();


        if (empty($contasareceber)) {
            Flash::error('Contasareceber not found');

            return redirect(route('contasarecebers.index'));
        }

        return view('contasarecebers.edit')->with('contasareceber', $contasareceber)->with('clientes', $clientes);
    }

    /**
     * Update the specified contasareceber in storage.
     *
     * @param int $id
     * @param UpdatecontasareceberRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecontasareceberRequest $request)
    {
        $contasareceber = $this->contasareceberRepository->find($id);

        if (empty($contasareceber)) {
            Flash::error('Contasareceber not found');

            return redirect(route('contasarecebers.index'));
        }

        $contasareceber = $this->contasareceberRepository->update($request->all(), $id);

        Flash::success('Contasareceber updated successfully.');

        return redirect(route('contasarecebers.index'));
    }

    /**
     * Remove the specified contasareceber from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $contasareceber = $this->contasareceberRepository->find($id);

        if (empty($contasareceber)) {
            Flash::error('Contasareceber not found');

            return redirect(route('contasarecebers.index'));
        }

        $this->contasareceberRepository->delete($id);

        Flash::success('Contasareceber deleted successfully.');

        return redirect(route('contasarecebers.index'));
    }
}
