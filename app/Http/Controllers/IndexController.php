<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB; 


class IndexController extends Controller
{
    public function index()
    {

      $curso1 = DB::table('cursos')
      ->select('cursos.*')
      ->where('cursos.id', 1)->where('cursos.status',1)
      ->get();

      $curso2 = DB::table('cursos')
      ->select('cursos.*')
      ->where('cursos.id', 2)->where('cursos.status',1)
      ->get();

      $curso3 = DB::table('cursos')
      ->select('cursos.*')
      ->where('cursos.id', 3)->where('cursos.status',1)
      ->get();

      $curso4 = DB::table('cursos')
      ->select('cursos.*')
      ->where('cursos.id', 4)->where('cursos.status',1)
      ->get();

      $curso5 = DB::table('cursos')
      ->select('cursos.*')
      ->where('cursos.id', 5)->where('cursos.status',1)
      ->get();

      $curso6 = DB::table('cursos')
      ->select('cursos.*')
      ->where('cursos.id', 6)->where('cursos.status',1)
      ->get();


      $curso7 = DB::table('cursos')
      ->select('cursos.*')
      ->where('cursos.id', 7)->where('cursos.status',1)
      ->get();


      $curso8 = DB::table('cursos')
      ->select('cursos.*')
      ->where('cursos.id', 8)->where('cursos.status',1)
      ->get();

      $blog1 = DB::table('blogs') 
      ->select('blogs.*')
      ->where('blogs.id',1)->where('status',1)->get();

      $blog2 = DB::table('blogs') 
      ->select('blogs.*')
      ->where('blogs.id',2)->where('status',1)->get();

      $blog3 = DB::table('blogs') 
      ->select('blogs.*')
      ->where('blogs.id',3)->where('status',1)->get();

      $blog4 = DB::table('blogs') 
      ->select('blogs.*')
      ->where('blogs.id',4)->where('status',1)->get();

      $banner1 = DB::table('banners') 
      ->select('banners.*')
      ->where('banners.id',1)->get();

      $banner2 = DB::table('banners') 
      ->select('banners.*')
      ->where('banners.id',2)->get();

      $banner3 = DB::table('banners') 
      ->select('banners.*')
      ->where('banners.id',3)->get();

      $banner4 = DB::table('banners') 
      ->select('banners.*')
      ->where('banners.id',4)->get();

      $cursos = DB::table('cursos')
      ->select('cursos.*')->get();

    
      $depoimento1  = DB::table('depoimentos')
       ->join('cursos', 'cursos.id', '=', 'depoimentos.curso_id')
       ->select('depoimentos.*','cursos.titulo')->where('depoimentos.curso_id',1)->where('depoimentos.status',1)->get();


       $depoimento2  = DB::table('depoimentos')
       ->join('cursos', 'cursos.id', '=', 'depoimentos.curso_id')
       ->select('depoimentos.*','cursos.titulo')->where('depoimentos.curso_id',2)->where('depoimentos.status',1)->get();


       $depoimento3  = DB::table('depoimentos')
       ->join('cursos', 'cursos.id', '=', 'depoimentos.curso_id')
       ->select('depoimentos.*','cursos.titulo')->where('depoimentos.curso_id',3)->where('depoimentos.status',1)->get();

       $depoimento4  = DB::table('depoimentos')
       ->join('cursos', 'cursos.id', '=', 'depoimentos.curso_id')
       ->select('depoimentos.*','cursos.titulo')->where('depoimentos.curso_id',4)->where('depoimentos.status',1)->get();

       $depoimento5  = DB::table('depoimentos')
       ->join('cursos', 'cursos.id', '=', 'depoimentos.curso_id')
       ->select('depoimentos.*','cursos.titulo')->where('depoimentos.curso_id',5)->where('depoimentos.status',1)->get();
     
       $depoimento6  = DB::table('depoimentos')
       ->join('cursos', 'cursos.id', '=', 'depoimentos.curso_id')
       ->select('depoimentos.*','cursos.titulo')->where('depoimentos.curso_id',6)->where('depoimentos.status',1)->get();

       $depoimento7  = DB::table('depoimentos')
       ->join('cursos', 'cursos.id', '=', 'depoimentos.curso_id')
       ->select('depoimentos.*','cursos.titulo')->where('depoimentos.curso_id',7)->where('depoimentos.status',1)->get();

       $depoimento8  = DB::table('depoimentos')
       ->join('cursos', 'cursos.id', '=', 'depoimentos.curso_id')
       ->select('depoimentos.*','cursos.titulo')->where('depoimentos.curso_id',8)->where('depoimentos.status',1)->get();





         return view('index')
         ->with(compact('curso1','curso2','curso3','curso4','curso5','curso6','curso7','curso8','blog1','blog2','blog3','blog4','banner1','banner2','banner3','banner4','cursos','depoimento1','depoimento2','depoimento3','depoimento4','depoimento5','depoimento6','depoimento7','depoimento8'));                  
    }


}

