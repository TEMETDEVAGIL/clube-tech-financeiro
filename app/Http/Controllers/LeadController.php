<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lead;
use Redirect;
use DB;  


class LeadController extends Controller
{
    public function save(Request $req){
    $lead = new Lead;
    $lead ->nome = $req->nome;
    $lead ->email = $req->email;
    $lead ->telefone = $req->telefone;
    $lead->assunto = $req->assunto;
    $lead->mensagem = $req->mensagem;
   
  
    $lead->save();
    return Redirect::to("/")->withFail('Email enviado com sucesso ! Aguarde o nosso contato.');
}
}
