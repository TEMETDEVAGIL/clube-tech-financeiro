<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatecursosRequest;
use App\Http\Requests\UpdatecursosRequest;
use App\Repositories\cursosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class cursosController extends AppBaseController
{
    /** @var  cursosRepository */
    private $cursosRepository;

    public function __construct(cursosRepository $cursosRepo)
    {
        $this->cursosRepository = $cursosRepo;
    }

    /**
     * Display a listing of the cursos.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $cursos = $this->cursosRepository->all();

        return view('cursos.index')
            ->with('cursos', $cursos);
    }

    /**
     * Show the form for creating a new cursos.
     *
     * @return Response
     */
    public function create()
    {
        return view('cursos.create');
    }

    /**
     * Store a newly created cursos in storage.
     *
     * @param CreatecursosRequest $request
     *
     * @return Response
     */
    public function store(CreatecursosRequest $request)
    {
        $input = $request->all();

        $cursos = $this->cursosRepository->create($input);

        Flash::success('Cursos saved successfully.');

        return redirect(route('cursos.index'));
    }

    /**
     * Display the specified cursos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cursos = $this->cursosRepository->find($id);

        if (empty($cursos)) {
            Flash::error('Cursos not found');

            return redirect(route('cursos.index'));
        }

        return view('cursos.show')->with('cursos', $cursos);
    }

    /**
     * Show the form for editing the specified cursos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cursos = $this->cursosRepository->find($id);

        if (empty($cursos)) {
            Flash::error('Cursos not found');

            return redirect(route('cursos.index'));
        }

        return view('cursos.edit')->with('cursos', $cursos);
    }

    /**
     * Update the specified cursos in storage.
     *
     * @param int $id
     * @param UpdatecursosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecursosRequest $request)
    {
        $cursos = $this->cursosRepository->find($id);

        if (empty($cursos)) {
            Flash::error('Cursos not found');

            return redirect(route('cursos.index'));
        }

        $cursos = $this->cursosRepository->update($request->all(), $id);

        Flash::success('Cursos updated successfully.');

        return redirect(route('cursos.index'));
    }

    /**
     * Remove the specified cursos from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cursos = $this->cursosRepository->find($id);

        if (empty($cursos)) {
            Flash::error('Cursos not found');

            return redirect(route('cursos.index'));
        }

        $this->cursosRepository->delete($id);

        Flash::success('Cursos deleted successfully.');

        return redirect(route('cursos.index'));
    }
}
