<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB; 


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          //clientes
          $clientes = DB::table('clientes')
          ->select('clientes.*')->get();
        
          //ClientesCOUNT
          $clientescount=count($clientes);  
 
          //empresas
          $empresas = DB::table('empresas')
          ->select('empresas.*')->get();
          
          //empresasCOUNT
          $empresascount=count($empresas); 
          
          //contas a pagar
          $contasapagar = DB::table('contasapagar')
          ->select('contasapagar.*')->where('status',0)->where('deleted_at',null)->get();
          
          //contasapagarCOUNT
          $contasapagarcount=count($contasapagar); 

          //contas a receber
          $contasareceber = DB::table('contasareceber')
          ->select('contasareceber.*')->where('status',0)->where('deleted_at',null)->get();
          
          //contasareceber COUNT
          $contasarecebercount=count($contasareceber); 
 
             
 
 
          return view('home')
          ->with(compact('clientescount','empresascount','contasapagarcount','contasarecebercount'));                      
    }
}
