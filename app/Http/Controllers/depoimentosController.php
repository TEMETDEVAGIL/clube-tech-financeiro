<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatedepoimentosRequest;
use App\Http\Requests\UpdatedepoimentosRequest;
use App\Repositories\depoimentosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class depoimentosController extends AppBaseController
{
    /** @var  depoimentosRepository */
    private $depoimentosRepository;

    public function __construct(depoimentosRepository $depoimentosRepo)
    {
        $this->depoimentosRepository = $depoimentosRepo;
    }

    /**
     * Display a listing of the depoimentos.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $depoimentos = $this->depoimentosRepository->all();

        return view('depoimentos.index')
            ->with('depoimentos', $depoimentos);
    }

    /**
     * Show the form for creating a new depoimentos.
     *
     * @return Response
     */
    public function create()
    {
        return view('depoimentos.create');
    }

    /**
     * Store a newly created depoimentos in storage.
     *
     * @param CreatedepoimentosRequest $request
     *
     * @return Response
     */
    public function store(CreatedepoimentosRequest $request)
    {
        $input = $request->all();

        $depoimentos = $this->depoimentosRepository->create($input);

        Flash::success('Depoimentos saved successfully.');

        return redirect(route('depoimentos.index'));
    }

    /**
     * Display the specified depoimentos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $depoimentos = $this->depoimentosRepository->find($id);

        if (empty($depoimentos)) {
            Flash::error('Depoimentos not found');

            return redirect(route('depoimentos.index'));
        }

        return view('depoimentos.show')->with('depoimentos', $depoimentos);
    }

    /**
     * Show the form for editing the specified depoimentos.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $depoimentos = $this->depoimentosRepository->find($id);

        if (empty($depoimentos)) {
            Flash::error('Depoimentos not found');

            return redirect(route('depoimentos.index'));
        }

        return view('depoimentos.edit')->with('depoimentos', $depoimentos);
    }

    /**
     * Update the specified depoimentos in storage.
     *
     * @param int $id
     * @param UpdatedepoimentosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatedepoimentosRequest $request)
    {
        $depoimentos = $this->depoimentosRepository->find($id);

        if (empty($depoimentos)) {
            Flash::error('Depoimentos not found');

            return redirect(route('depoimentos.index'));
        }

        $depoimentos = $this->depoimentosRepository->update($request->all(), $id);

        Flash::success('Depoimentos updated successfully.');

        return redirect(route('depoimentos.index'));
    }

    /**
     * Remove the specified depoimentos from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $depoimentos = $this->depoimentosRepository->find($id);

        if (empty($depoimentos)) {
            Flash::error('Depoimentos not found');

            return redirect(route('depoimentos.index'));
        }

        $this->depoimentosRepository->delete($id);

        Flash::success('Depoimentos deleted successfully.');

        return redirect(route('depoimentos.index'));
    }
}
