<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateleadsRequest;
use App\Http\Requests\UpdateleadsRequest;
use App\Repositories\leadsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class leadsController extends AppBaseController
{
    /** @var  leadsRepository */
    private $leadsRepository;

    public function __construct(leadsRepository $leadsRepo)
    {
        $this->leadsRepository = $leadsRepo;
    }

    /**
     * Display a listing of the leads.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $leads = $this->leadsRepository->all();

        return view('leads.index')
            ->with('leads', $leads);
    }

    /**
     * Show the form for creating a new leads.
     *
     * @return Response
     */
    public function create()
    {
        return view('leads.create');
    }

    /**
     * Store a newly created leads in storage.
     *
     * @param CreateleadsRequest $request
     *
     * @return Response
     */
    public function store(CreateleadsRequest $request)
    {
        $input = $request->all();

        $leads = $this->leadsRepository->create($input);

        Flash::success('Leads saved successfully.');

        return redirect(route('leads.index'));
    }

    /**
     * Display the specified leads.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $leads = $this->leadsRepository->find($id);

        if (empty($leads)) {
            Flash::error('Leads not found');

            return redirect(route('leads.index'));
        }

        return view('leads.show')->with('leads', $leads);
    }

    /**
     * Show the form for editing the specified leads.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $leads = $this->leadsRepository->find($id);

        if (empty($leads)) {
            Flash::error('Leads not found');

            return redirect(route('leads.index'));
        }

        return view('leads.edit')->with('leads', $leads);
    }

    /**
     * Update the specified leads in storage.
     *
     * @param int $id
     * @param UpdateleadsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateleadsRequest $request)
    {
        $leads = $this->leadsRepository->find($id);

        if (empty($leads)) {
            Flash::error('Leads not found');

            return redirect(route('leads.index'));
        }

        $leads = $this->leadsRepository->update($request->all(), $id);

        Flash::success('Leads updated successfully.');

        return redirect(route('leads.index'));
    }

    /**
     * Remove the specified leads from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $leads = $this->leadsRepository->find($id);

        if (empty($leads)) {
            Flash::error('Leads not found');

            return redirect(route('leads.index'));
        }

        $this->leadsRepository->delete($id);

        Flash::success('Leads deleted successfully.');

        return redirect(route('leads.index'));
    }
}
