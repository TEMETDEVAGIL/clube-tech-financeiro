<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contasapagar extends Model
{
    protected $fillable = [
        'empresa_id', 'datadepagamento', 'datadevencimento','valor','status'
    ];
}
