<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class depoimentos
 * @package App\Models
 * @version May 20, 2020, 5:59 pm UTC
 *
 * @property \App\Models\Curso $curso
 * @property string $email
 * @property string $nome
 * @property integer $curso_id
 * @property string $comentario
 * @property string $nota
 * @property boolean $status
 */
class depoimentos extends Model
{
    use SoftDeletes;

    public $table = 'depoimentos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'email',
        'nome',
        'curso_id',
        'comentario',
        'nota',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'email' => 'string',
        'nome' => 'string',
        'curso_id' => 'integer',
        'comentario' => 'string',
        'nota' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'email' => 'required',
        'nome' => 'required',
        'curso_id' => 'required',
        'comentario' => 'required',
        'nota' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function curso()
    {
        return $this->belongsTo(\App\Models\Curso::class, 'curso_id');
    }
}
