<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class clientes
 * @package App\Models
 * @version February 19, 2020, 12:30 pm UTC
 *
 * @property string razaosocial
 * @property string nome
 * @property string cpf
 * @property string cnpj
 * @property string email
 * @property string telefone
 * @property string cep
 * @property string logradouro
 * @property string cidade
 * @property string bairro
 * @property string uf
 * @property string numero
 * @property string complemento
 */
class clientes extends Model
{
    use SoftDeletes;

    public $table = 'clientes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'razaosocial',
        'nome',
        'cpf',
        'cnpj',
        'email',
        'telefone',
        'cep',
        'logradouro',
        'cidade',
        'bairro',
        'uf',
        'numero',
        'complemento'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'razaosocial' => 'string',
        'nome' => 'string',
        'cpf' => 'string',
        'cnpj' => 'string',
        'email' => 'string',
        'telefone' => 'string',
        'cep' => 'string',
        'logradouro' => 'string',
        'cidade' => 'string',
        'bairro' => 'string',
        'uf' => 'string',
        'numero' => 'string',
        'complemento' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
