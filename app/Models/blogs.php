<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class blogs
 * @package App\Models
 * @version May 6, 2020, 4:56 pm UTC
 *
 * @property string titulo
 * @property string imagem
 * @property string data
 * @property string link
 * @property boolean status
 */
class blogs extends Model
{
    use SoftDeletes;

    public $table = 'blogs';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'titulo',
        'imagem',
        'data',
        'link',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'titulo' => 'string',
        'imagem' => 'string',
        'data' => 'string',
        'link' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'titulo' => 'required',
        'imagem' => 'required',
        'data' => 'required',
        'link' => 'required',
        'status' => 'required'
    ];

    
}
