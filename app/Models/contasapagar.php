<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class contasapagar
 * @package App\Models
 * @version April 30, 2020, 4:13 pm UTC
 *
 * @property \App\Models\Empresa empresa
 * @property integer empresa_id
 * @property string datavencimento
 * @property string datapagamento
 * @property number valor
 * @property boolean status
 */
class contasapagar extends Model
{
    use SoftDeletes;

    public $table = 'contasapagar';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'empresa_id',
        'datavencimento',
        'datapagamento',
        'valor',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'empresa_id' => 'integer',
        'datavencimento' => 'string',
        'datapagamento' => 'string',
        'valor' => 'float',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'empresa_id' => 'required',
        'datavencimento' => 'required',
        'datapagamento' => 'required',
        'valor' => 'required',
        'status' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function empresa()
    {
        return $this->belongsTo(\App\Models\Empresa::class, 'empresa_id');
    }
}
