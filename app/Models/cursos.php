<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cursos
 * @package App\Models
 * @version May 13, 2020, 4:51 pm UTC
 *
 * @property \App\Models\Professor $professor
 * @property string $titulo
 * @property string $fraseefeito
 * @property string $minidescricao
 * @property string $descricao
 * @property string $data
 * @property string $cargahoraria
 * @property string $conteudo
 * @property string $requisitos
 * @property string $publico
 * @property string $modulo1
 * @property string $modulo2
 * @property string $modulo3
 * @property string $modulo4
 * @property string $modulo5
 * @property string $modulo6
 * @property string $modulo7
 * @property string $modulo8
 * @property string $modulo9
 * @property string $modulo10
 * @property string $valor
 * @property integer $professor_id
 * @property string $link
 * @property string $imagem
 * @property string $banner
 * @property string $contadormes
 * @property string $contadordia
 * @property string $contadorano
 * @property string $contadorhorario
 * @property boolean $status
 */
class cursos extends Model
{
    use SoftDeletes;

    public $table = 'cursos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'titulo',
        'fraseefeito',
        'minidescricao',
        'descricao',
        'data',
        'cargahoraria',
        'conteudo',
        'requisitos',
        'publico',
        'modulo1',
        'modulo2',
        'modulo3',
        'modulo4',
        'modulo5',
        'modulo6',
        'modulo7',
        'modulo8',
        'modulo9',
        'modulo10',
        'valor',
        'professor_id',
        'link',
        'imagem',
        'banner',
        'contadormes',
        'contadordia',
        'contadorano',
        'contadorhorario',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'titulo' => 'string',
        'fraseefeito' => 'string',
        'minidescricao' => 'string',
        'descricao' => 'string',
        'data' => 'string',
        'cargahoraria' => 'string',
        'conteudo' => 'string',
        'requisitos' => 'string',
        'publico' => 'string',
        'modulo1' => 'string',
        'modulo2' => 'string',
        'modulo3' => 'string',
        'modulo4' => 'string',
        'modulo5' => 'string',
        'modulo6' => 'string',
        'modulo7' => 'string',
        'modulo8' => 'string',
        'modulo9' => 'string',
        'modulo10' => 'string',
        'valor' => 'string',
        'professor_id' => 'integer',
        'link' => 'string',
        'imagem' => 'string',
        'banner' => 'string',
        'contadormes' => 'string',
        'contadordia' => 'string',
        'contadorano' => 'string',
        'contadorhorario' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'titulo' => 'required',
        'minidescricao' => 'required',
        'descricao' => 'required',
        'data' => 'required',
        'cargahoraria' => 'required',
        'conteudo' => 'required',
        'publico' => 'required',
        'valor' => 'required',
        'professor_id' => 'required',
        'imagem' => 'required',
        'banner' => 'required',
        'contadormes' => 'required',
        'contadordia' => 'required',
        'contadorano' => 'required',
        'contadorhorario' => 'required',
        'status' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function professor()
    {
        return $this->belongsTo(\App\Models\Professor::class, 'professor_id');
    }
}
