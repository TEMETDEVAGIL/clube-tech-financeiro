<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class contasareceber
 * @package App\Models
 * @version April 30, 2020, 6:02 pm UTC
 *
 * @property \App\Models\Cliente cliente
 * @property integer cliente_id
 * @property string datarecebimento
 * @property number valor
 * @property boolean status
 */
class contasareceber extends Model
{
    use SoftDeletes;

    public $table = 'contasareceber';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'cliente_id',
        'datarecebimento',
        'valor',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'cliente_id' => 'integer',
        'datarecebimento' => 'string',
        'valor' => 'float',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'cliente_id' => 'required',
        'datarecebimento' => 'required',
        'valor' => 'required',
        'status' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function cliente()
    {
        return $this->belongsTo(\App\Models\Cliente::class, 'cliente_id');
    }
}
