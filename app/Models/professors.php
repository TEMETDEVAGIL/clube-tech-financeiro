<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class professors
 * @package App\Models
 * @version May 8, 2020, 2:40 pm UTC
 *
 * @property string $nome
 * @property string $cpf
 * @property string $telefone
 * @property string $email
 * @property string $minicv
 * @property string $imagem
 * @property string $cep
 * @property string $logradouro
 * @property string $bairro
 * @property string $cidade
 * @property string $uf
 * @property string $complemento
 * @property string $banco
 * @property string $agencia
 * @property string $conta
 */
class professors extends Model
{
    use SoftDeletes;

    public $table = 'professors';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nome',
        'cpf',
        'telefone',
        'email',
        'minicv',
        'imagem',
        'cep',
        'logradouro',
        'bairro',
        'cidade',
        'uf',
        'complemento',
        'banco',
        'agencia',
        'conta'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nome' => 'string',
        'cpf' => 'string',
        'telefone' => 'string',
        'email' => 'string',
        'minicv' => 'string',
        'imagem' => 'string',
        'cep' => 'string',
        'logradouro' => 'string',
        'bairro' => 'string',
        'cidade' => 'string',
        'uf' => 'string',
        'complemento' => 'string',
        'banco' => 'string',
        'agencia' => 'string',
        'conta' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nome' => 'required',
        'cpf' => 'required',
        'telefone' => 'required',
        'email' => 'required',
        'minicv' => 'required',
        'cep' => 'required',
        'logradouro' => 'required',
        'bairro' => 'required',
        'cidade' => 'required',
        'uf' => 'required',
        'complemento' => 'required'
    ];

    
}
