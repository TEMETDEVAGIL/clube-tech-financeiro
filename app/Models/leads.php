<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class leads
 * @package App\Models
 * @version May 6, 2020, 3:26 pm UTC
 *
 * @property string nome
 * @property string email
 * @property string telefone
 * @property string assunto
 * @property string mensagem
 * @property boolean status
 */
class leads extends Model
{
    use SoftDeletes;

    public $table = 'leads';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nome',
        'email',
        'telefone',
        'assunto',
        'mensagem',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nome' => 'string',
        'email' => 'string',
        'telefone' => 'string',
        'assunto' => 'string',
        'mensagem' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nome' => 'required',
        'email' => 'required',
        'telefone' => 'required',
        'assunto' => 'required',
        'mensagem' => 'required'
    ];

    
}
