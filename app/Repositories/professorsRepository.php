<?php

namespace App\Repositories;

use App\Models\professors;
use App\Repositories\BaseRepository;

/**
 * Class professorsRepository
 * @package App\Repositories
 * @version May 8, 2020, 2:40 pm UTC
*/

class professorsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nome',
        'cpf',
        'telefone',
        'email',
        'minicv',
        'imagem',
        'cep',
        'logradouro',
        'bairro',
        'cidade',
        'uf',
        'complemento',
        'banco',
        'agencia',
        'conta'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return professors::class;
    }
}
