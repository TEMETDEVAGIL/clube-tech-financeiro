<?php

namespace App\Repositories;

use App\Models\blogs;
use App\Repositories\BaseRepository;

/**
 * Class blogsRepository
 * @package App\Repositories
 * @version May 6, 2020, 4:56 pm UTC
*/

class blogsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'titulo',
        'imagem',
        'data',
        'link',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return blogs::class;
    }
}
