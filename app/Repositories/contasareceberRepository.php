<?php

namespace App\Repositories;

use App\Models\contasareceber;
use App\Repositories\BaseRepository;

/**
 * Class contasareceberRepository
 * @package App\Repositories
 * @version April 30, 2020, 6:02 pm UTC
*/

class contasareceberRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'cliente_id',
        'datarecebimento',
        'valor',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return contasareceber::class;
    }
}
