<?php

namespace App\Repositories;

use App\Models\aluno;
use App\Repositories\BaseRepository;

/**
 * Class alunoRepository
 * @package App\Repositories
 * @version May 18, 2020, 3:10 pm UTC
*/

class alunoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nome',
        'email',
        'telefone'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return aluno::class;
    }
}
