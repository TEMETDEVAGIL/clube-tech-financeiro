<?php

namespace App\Repositories;

use App\Models\banners;
use App\Repositories\BaseRepository;

/**
 * Class bannersRepository
 * @package App\Repositories
 * @version May 5, 2020, 4:10 pm UTC
*/

class bannersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'titulo',
        'imagem'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return banners::class;
    }
}
