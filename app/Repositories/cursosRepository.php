<?php

namespace App\Repositories;

use App\Models\cursos;
use App\Repositories\BaseRepository;

/**
 * Class cursosRepository
 * @package App\Repositories
 * @version May 13, 2020, 4:51 pm UTC
*/

class cursosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'titulo',
        'fraseefeito',
        'minidescricao',
        'descricao',
        'data',
        'cargahoraria',
        'conteudo',
        'requisitos',
        'publico',
        'modulo1',
        'modulo2',
        'modulo3',
        'modulo4',
        'modulo5',
        'modulo6',
        'modulo7',
        'modulo8',
        'modulo9',
        'modulo10',
        'valor',
        'professor_id',
        'link',
        'imagem',
        'banner',
        'contadormes',
        'contadordia',
        'contadorano',
        'contadorhorario',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cursos::class;
    }
}
