<?php

namespace App\Repositories;

use App\Models\leads;
use App\Repositories\BaseRepository;

/**
 * Class leadsRepository
 * @package App\Repositories
 * @version May 6, 2020, 3:26 pm UTC
*/

class leadsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nome',
        'email',
        'telefone',
        'assunto',
        'mensagem',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return leads::class;
    }
}
