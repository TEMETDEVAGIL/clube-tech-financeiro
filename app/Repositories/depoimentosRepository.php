<?php

namespace App\Repositories;

use App\Models\depoimentos;
use App\Repositories\BaseRepository;

/**
 * Class depoimentosRepository
 * @package App\Repositories
 * @version May 20, 2020, 5:59 pm UTC
*/

class depoimentosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'email',
        'nome',
        'curso_id',
        'comentario',
        'nota',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return depoimentos::class;
    }
}
