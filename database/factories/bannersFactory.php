<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\banners;
use Faker\Generator as Faker;

$factory->define(banners::class, function (Faker $faker) {

    return [
        'titulo' => $faker->word,
        'imagem' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
