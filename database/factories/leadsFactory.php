<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\leads;
use Faker\Generator as Faker;

$factory->define(leads::class, function (Faker $faker) {

    return [
        'nome' => $faker->word,
        'email' => $faker->word,
        'telefone' => $faker->word,
        'assunto' => $faker->word,
        'mensagem' => $faker->word,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
