<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\professors;
use Faker\Generator as Faker;

$factory->define(professors::class, function (Faker $faker) {

    return [
        'nome' => $faker->word,
        'cpf' => $faker->word,
        'telefone' => $faker->word,
        'email' => $faker->word,
        'minicv' => $faker->word,
        'imagem' => $faker->word,
        'cep' => $faker->word,
        'logradouro' => $faker->word,
        'bairro' => $faker->word,
        'cidade' => $faker->word,
        'uf' => $faker->word,
        'complemento' => $faker->word,
        'banco' => $faker->word,
        'agencia' => $faker->word,
        'conta' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
