<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\contasapagar;
use Faker\Generator as Faker;

$factory->define(contasapagar::class, function (Faker $faker) {

    return [
        'empresa_id' => $faker->word,
        'datavencimento' => $faker->word,
        'datapagamento' => $faker->word,
        'valor' => $faker->randomDigitNotNull,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
