<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\depoimentos;
use Faker\Generator as Faker;

$factory->define(depoimentos::class, function (Faker $faker) {

    return [
        'email' => $faker->word,
        'nome' => $faker->word,
        'curso_id' => $faker->word,
        'comentario' => $faker->word,
        'nota' => $faker->word,
        'status' => $faker->word,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
