<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\aluno;
use Faker\Generator as Faker;

$factory->define(aluno::class, function (Faker $faker) {

    return [
        'nome' => $faker->word,
        'email' => $faker->word,
        'telefone' => $faker->word,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
