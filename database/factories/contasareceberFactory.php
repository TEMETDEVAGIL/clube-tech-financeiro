<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\contasareceber;
use Faker\Generator as Faker;

$factory->define(contasareceber::class, function (Faker $faker) {

    return [
        'cliente_id' => $faker->word,
        'datarecebimento' => $faker->word,
        'valor' => $faker->randomDigitNotNull,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
