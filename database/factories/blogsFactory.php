<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\blogs;
use Faker\Generator as Faker;

$factory->define(blogs::class, function (Faker $faker) {

    return [
        'titulo' => $faker->word,
        'imagem' => $faker->word,
        'data' => $faker->word,
        'link' => $faker->word,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
