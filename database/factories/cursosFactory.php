<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\cursos;
use Faker\Generator as Faker;

$factory->define(cursos::class, function (Faker $faker) {

    return [
        'titulo' => $faker->word,
        'fraseefeito' => $faker->word,
        'minidescricao' => $faker->word,
        'descricao' => $faker->word,
        'data' => $faker->word,
        'cargahoraria' => $faker->word,
        'conteudo' => $faker->word,
        'requisitos' => $faker->word,
        'publico' => $faker->word,
        'modulo1' => $faker->word,
        'modulo2' => $faker->word,
        'modulo3' => $faker->word,
        'modulo4' => $faker->word,
        'modulo5' => $faker->word,
        'modulo6' => $faker->word,
        'modulo7' => $faker->word,
        'modulo8' => $faker->word,
        'modulo9' => $faker->word,
        'modulo10' => $faker->word,
        'valor' => $faker->word,
        'professor_id' => $faker->word,
        'link' => $faker->word,
        'imagem' => $faker->word,
        'banner' => $faker->word,
        'contadormes' => $faker->word,
        'contadordia' => $faker->word,
        'contadorano' => $faker->word,
        'contadorhorario' => $faker->word,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
