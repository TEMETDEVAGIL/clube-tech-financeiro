<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaCursos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titulo');
            $table->string('fraseefeito')->nullable();
            $table->string('minidescricao');
            $table->string('descricao');
            $table->string('data');
            $table->string('cargahoraria');
            $table->string('conteudo');
            $table->string('requisitos')->nullable();
            $table->string('publico');
            $table->string('modulo1')->nullable();
            $table->string('modulo2')->nullable();
            $table->string('modulo3')->nullable();
            $table->string('modulo4')->nullable();
            $table->string('modulo5')->nullable();
            $table->string('modulo6')->nullable();
            $table->string('modulo7')->nullable();
            $table->string('modulo8')->nullable();
            $table->string('modulo9')->nullable();
            $table->string('modulo10')->nullable();
            $table->string('valor');
            $table->bigInteger('professor_id')->unsigned();  
            $table->foreign('professor_id')->references('id')->on('professors');
            $table->string('link')->nullable();
            $table->string('imagem');
            $table->string('banner');
            $table->string('contadormes');
            $table->string('contadordia');
            $table->string('contadorano');
            $table->string('contadorhorario');
            $table->boolean('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cursos');
    }
}
